<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
Auth::routes();

Route::get('/', [HomeController::class, 'category']);
Route::get('/index', [HomeController::class, 'category']);
Route::get('/home', [HomeController::class, 'category']);

Route::view('forgot_password', 'auth.reset_password')->name('password.reset');

Route::get('/category/{category?}', [HomeController::class, 'category']);
Route::get('category/listAjax/{category}', [HomeController::class, 'categoryListAjax']);
Route::post('category/save', [HomeController::class, 'categorySave']);
Route::get('deleteCategory/{category_id}', [HomeController::class, 'deleteCategory']);
Route::get('getCategory/{category_id}', [HomeController::class, 'getCategory']);
Route::get('getCategory/{category_id}', [HomeController::class, 'getCategory']);

Route::get('/product/{status}', [HomeController::class, 'product']);
Route::post('/addNewProduct/save', [HomeController::class, 'addProductSave']);
Route::get('product/listAjax/{status}', [HomeController::class, 'productListAjax']);
Route::get('deleteProduct/{product_id}', [HomeController::class, 'deleteProduct']);
Route::get('getProduct/{product_id}', [HomeController::class, 'getProduct']);

Route::get('/zayavkiNaRazbor', [HomeController::class, 'zayavkiNaRazbor']);
Route::get('/zayavkiNaRazbor/listAjax', [HomeController::class, 'zayavkiNaRazborAjax']);
Route::get('/zayavkaNaRazborPrinyat/{id}', [HomeController::class, 'zayavkaNaRazborPrinyat']);

Route::get('users/profil', [HomeController::class, 'usersProfil']);
Route::get('users/profilListAjax', [HomeController::class, 'usersProfilListAjax']);
Route::get('getUserData/{user_id}', [HomeController::class, 'getUserData']);
Route::post('user/profil/editsave', [HomeController::class, 'userEditSave']);

Route::get('/profil', [HomeController::class, 'profil']);
Route::get('getUser/{user_id}', [HomeController::class, 'getUser']);
Route::post('editUser/save', [HomeController::class, 'editUserSave']);

//Route::get('/zaprosiNaReg', [HomeController::class, 'zaprosiNaReg']);
Route::get('getuserZaprosiNaReg/listAjax', [HomeController::class, 'getuserZaprosiNaReg']);
Route::get('acceptUserForLogin/{role_id}/{user_id}', [HomeController::class, 'acceptUserForLogin']);

Route::post('/addNewIconKachestva/save', [HomeController::class, 'addNewIconKachestva']);
Route::get('show/others', [HomeController::class, 'showOthers']);
Route::get('get/others', [HomeController::class, 'getOthers_ajax']);
Route::get('get/statuses/{id}', [HomeController::class, 'getStatuses']);
Route::post('edit/statuses', [HomeController::class, 'editStatues']);
Route::post('add/new/statuses', [HomeController::class, 'addNewStatus']);
Route::get( 'get/category/tree', [HomeController::class, 'getCategory']);

Route::get('otherDelete/{data_id}', [HomeController::class, 'otherDelete']);



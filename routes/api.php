<?php

use App\Http\Controllers\Api\HomeController;
use App\Http\Controllers\Api\PassportAuthController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('register', [PassportAuthController::class, 'register']);
Route::post('login', [PassportAuthController::class, 'login']);

Route::post('password/forgot', [UserController::class,'forgot']);
Route::post('password/reset', [UserController::class,'reset']);

Route::middleware('auth:api')->group(function () {
    Route::post('logout', [PassportAuthController::class, 'logout']);
    Route::post('change/password', [UserController::class, 'changePassword']);
    Route::get('profile', [UserController::class, 'profile']);
    Route::get('new/parsing', [HomeController::class, 'newInParsing']);
    Route::post('send/toParsing', [HomeController::class, 'sendToParsing']);
    Route::post('save/toChosen', [HomeController::class, 'saveChosen']);
    Route::post('delete/fromChosen', [HomeController::class, 'delChosen']);
    Route::get('show/chosen', [HomeController::class, 'getChosen']);
    Route::get('get/all/product', [HomeController::class, 'getProduct']);
    Route::get('get/category/tree', [HomeController::class, 'getCategory']);
    Route::get('get/designation', [HomeController::class, 'getDesignation']);
    Route::post('get/product', [HomeController::class, 'getOneProduct']);
    Route::post('get/product/byCategory', [HomeController::class, 'getProductByCategory']);
    Route::post('getCreate/subscribe', [HomeController::class, 'getCreateSubscriber']);

});

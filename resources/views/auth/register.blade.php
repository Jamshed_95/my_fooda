
<!DOCTYPE html>
<html dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('img/icon.png')}}">
    <title>Fooda - Регистрация</title>
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{url('adminmart/sweetalert2/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
    <!-- Custom CSS -->
    <link href="{{url('adminmart/dist/css/style.min.css')}}" rel="stylesheet">
</head>

<body>
<div class="main-wrapper">
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

    <div class="auth-wrapper d-flex no-block justify-content-center align-items-center position-relative"
         style="background:url({{url('adminmart/assets/images/big/auth-bg.jpg')}}) no-repeat center center;">
        <div class="row justify-content-center">
            <div class="col-md-4 bg-white">
                <div class="p-3">
                    <h2 class="mt-3 text-center text-success">FOODA</h2>
                    <p class="text-center">Регистрация для получения доступа к панели администратора</p>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="text-dark" for="email">Email<sup class="text-danger">*</sup></label>
                                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"
                                           placeholder="Ваш адрес электронной почты" required autocomplete="email">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group mb-0">
                                    <label class="text-dark" for="password">Пароль<sup class="text-danger">*</sup></label>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                                           name="password" required autocomplete="password" placeholder="Пароль">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <p align="left" class="text-muted  mt-0"><small>Пароль должен содержать от 8 до 30 знаков</small></p>
                            </div>
                            <div class="col-lg-12 text-center mt-2">
                                <button type="submit" class="btn btn-block btn-success">Зарегистрироватся <i class="fa fa-sign-in-alt float-right mt-1"></i></button>
                            </div>
                            <div class="col-lg-12 mt-5">
                                <span class="text-left">Уже есть аккаунт?</span> <a href="{{url('login')}}" class="text-primary float-right">Войти</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@if(Session::get('infoReg'))
    <input type="hidden" id="infoReg">
@endif
<script src="{{url('adminmart/assets/libs/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{url('adminmart/sweetalert2/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{url('adminmart/assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
<script src="{{url('adminmart/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script>
    $(".preloader ").fadeOut();
    if ($('#infoReg').length > 0) {
        Swal.fire({
            type: 'warning',
            title: 'Регистрация прошла успешна\n Ожидайте получение прав доступа!',
        })
    }
</script>
</body>

</html>

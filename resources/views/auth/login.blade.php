
<!DOCTYPE html>
<html dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('img/icon.png')}}">
    <title>Fooda - Вход в админку</title>
    <!-- Custom CSS -->
    <link href="{{url('adminmart/dist/css/style.min.css')}}" rel="stylesheet">
</head>

<body>
<div class="main-wrapper">
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

    <div class="auth-wrapper d-flex no-block justify-content-center align-items-center position-relative" style="background:url({{url('adminmart/assets/images/big/auth-bg.jpg')}}) no-repeat center center;">
        <div class="row justify-content-center">
            <div class="col-md-5 bg-white">
                <div class="p-3">
                    <h2 class="mt-3 text-center text-success">FOODA</h2>
                    <p class="text-center">Введите свой адрес электронной почты и пароль для доступа к панели администратора</p>
                    @if ($errors->has('email'))
                        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>{{ $errors->first('email') }}
                        </div>
                    @endif
                    @if ($errors->has('password'))
                        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>{{ $errors->first('password') }}
                        </div>
                    @endif
                    <form action="{{ route('login') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="text-dark" for="email">Email</label>
                                    <input class="form-control" id="email" type="text" name="email" placeholder="Введите ваш email">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="text-dark" for="password">Пароль</label>
                                    <input class="form-control" id="password" type="password" name="password" placeholder="Введите ваш пароль">
                                </div>
                            </div>
                            <div class="col-lg-12 text-center">
                                <button type="submit" class="btn btn-block btn-success">Войти <i class="fa fa-sign-in-alt float-right mt-1"></i></button>
                            </div>
                            <div class="col-lg-12 mt-5">
                                <span class="text-left"> Нет учетной записи?</span> <a href="{{url('register')}}" class="text-danger float-right">Зарегистрироваться</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Login box.scss -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- All Required js -->
<!-- ============================================================== -->
<script src="{{url('adminmart/assets/libs/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{url('adminmart/assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
<script src="{{url('adminmart/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- ============================================================== -->
<!-- This page plugin js -->
<!-- ============================================================== -->
<script>
    $(".preloader ").fadeOut();
</script>
</body>

</html>

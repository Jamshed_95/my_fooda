<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('img/icon.png')}}">
    <title>Fooda | Админка</title>
    <!-- Custom CSS -->
    @stack('style_before')
    <link href="{{url('adminmart/assets/extra-libs/c3/c3.min.css')}}" rel="stylesheet">
    <!-- Select2 -->
{{--    <link rel="stylesheet" href="{{url('select2_new/select2.min.css')}}">--}}
{{--    <link rel="stylesheet" href="{{url('select2_new/select2-bootstrap4.min.css')}}">--}}
    <!-- Select2 -->
    <link rel="stylesheet" href="{{url('css/select2.min.css')}}">
    <link rel="stylesheet" href="{{url('css/select2-bootstrap4.min.css')}}">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{url('adminmart/sweetalert2/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
    <!-- This page plugin CSS -->
    <link href="{{url('adminmart/assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{url('adminmart/dist/css/style.min.css')}}" rel="stylesheet">
    @stack('style_after')
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
        function goto(type){
            if (type=='category'){
                window.location.href = "{{url('index')}}";
            }
            if (type=='product'){
                window.location.href = "{{url('product')}}";
            }
        }
    </script>
    <style>
        .page-item {
            margin-right: 10px;
            margin-left: 10px;
        }
        .page-item.active .page-link {
            border-radius: 5px;
        }
    </style>
</head>

<body class="ls-closed">
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="mini-sidebar"
     data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full" class="mini-sidebar" style="width: 100%">
    <header class="topbar" data-navbarbg="skin6">
        <nav class="navbar top-navbar navbar-expand-md">
            <div class="navbar-header" data-logobg="skin6" style="width: 230px;">
                <div class="navbar-brand pr-0">
                    <a href="{{url('index')}}">
                        <b class="logo-icon">
                            <img src="{{url('img/icon.png')}}" alt="homepage" class="dark-logo" style="width: 50px; height: 50px" />
                            <img src="{{url('img/icon.png')}}" alt="homepage" class="light-logo" style="width: 50px; height: 50px" />
                        </b>
                        <span class="logo-text text-dark">
                          F&nbsp; O&nbsp; O&nbsp; D&nbsp; A
                        </span>
                    </a>
                </div>
            </div>
            <div class="navbar-collapse collapse bg-white text-center" id="navbarSupportedContent" style="margin-left: 230px">
                <ul class="navbar-nav mr-auto ml-3 pl-1 text-center">
                    @php
                        $isOpen = (Request::is('category/*') || Request::is('index') || Request::is('home') || Request::is('/') || Request::is('category') );
                    @endphp
                    <a class="btn waves-effect waves-light btn-outline-primary {{$isOpen?'active':''}}"  href="{{url('/category')}}" aria-expanded="false"><i data-feather="tag" class="feather-icon"></i><span class="hide-menu">Категории</span></a>
                    @php
                        $isOpen1 = (Request::is('product/*') || Request::is('product'));
                    @endphp
                    &nbsp;&nbsp;<a class="btn waves-effect waves-light btn-outline-primary {{$isOpen1?'active':''}}"  href="{{url('/product/all')}}"  aria-expanded="false"><i data-feather="grid" class="feather-icon"></i><span class="hide-menu">Продукты</span></a>
                    @php
                        $isOpen2 = (Request::is('zayavkiNaRazbor'));
                    @endphp
                    &nbsp;&nbsp;<a class="btn waves-effect waves-light btn-outline-primary {{$isOpen2?'active':''}}" href="{{url('zayavkiNaRazbor')}}" aria-expanded="false"><i data-feather="file-text" class="feather-icon"></i>
                        <span class="hide-menu">Заявки</span>
                    </a>
                    @php
                        $isOpen3 = (Request::is('users/profil'));
                    @endphp
                    &nbsp;&nbsp;<a class="btn waves-effect waves-light btn-outline-primary {{$isOpen3?'active':''}}" href="{{url('users/profil')}}" aria-expanded="false"><i data-feather="users" class="feather-icon"></i>
                        <span class="hide-menu">Пользователи</span>
                    </a>
                    @php
                        $isOpen4 = (Request::is('profil'));
                    @endphp
                    &nbsp;&nbsp;<a class="btn waves-effect waves-light btn-outline-primary {{$isOpen4?'active':''}}" href="{{url('/profil')}}" aria-expanded="false"><i data-feather="edit-3" class="feather-icon"></i>
                        <span class="hide-menu">Профиль </span>
                    </a>
                    {{--@php
                        $isOpen5 = (Request::is('zaprosiNaReg'));
                    @endphp
                    &nbsp;&nbsp;<a class="btn waves-effect waves-light btn-outline-primary {{$isOpen5?'active':''}}" href="{{url('/zaprosiNaReg')}}" aria-expanded="false"><i data-feather="check-circle" class="feather-icon"></i>
                        <span class="hide-menu"> Подт. регистрации</span>
                    </a>--}}
                    @php
                        $isOpen6 = (Request::is('show/others'));
                    @endphp
                    &nbsp;&nbsp;<a class="btn waves-effect waves-light btn-outline-primary {{$isOpen6?'active':''}}" href="{{url('/show/others')}}" aria-expanded="false"><i data-feather="edit-3" class="feather-icon"></i>
                        <span class="hide-menu">Разное</span>
                    </a>
                </ul>
                <ul class="navbar-nav float-right">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            <img src="{{url('img/user.png')}}" alt="user" class="rounded-circle" width="40">
                            <span class="ml-2 d-none d-lg-inline-block"> <span class="text-dark">{{ Auth::user()->email }}</span> <i data-feather="chevron-down" class="svg-icon"></i></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                            <a class="dropdown-item mt-2">
                                <i class="fa fa-cogs mr-2 ml-1"></i> {{ Auth::user()->role->name }}
                            </a>
                            <a class="dropdown-item mt-2" href="{{url('/profil')}}">
                                <i data-feather="edit-3" class="svg-icon mr-2 ml-1"></i> Профиль
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="{{ route('logout') }}" class="dropdown-item text-danger"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i data-feather="power" class="svg-icon mr-2 ml-1"></i> Выход
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <div class="page-wrapper m-0" style="width: 100%">
        @yield('content')
    </div>
    <footer class="footer text-center text-muted">FOODA</footer>
</div>
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="{{url('adminmart/assets/libs/jquery/dist/jquery.min.js')}}"></script>
<script src="{{url('adminmart/assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
<script src="{{url('adminmart/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{url('adminmart/dist/js/app-style-switcher.js')}}"></script>
<script src="{{url('adminmart/dist/js/feather.min.js')}}"></script>
<script src="{{url('adminmart/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
<script src="{{url('adminmart/dist/js/sidebarmenu.js')}}"></script>
<!-- Select2 -->
{{--<script src="{{url('select2_new/select2.full.min.js')}}"></script>--}}
{{--<script src="{{url('js/dist/js/custom.min.js')}}"></script>--}}
<!--This page JavaScript -->
<script src="{{url('adminmart/dist/js/pages/dashboards/dashboard1.min.js')}}"></script>
<!-- Select2 -->
<script src="{{url('js/select2.full.min.js')}}"></script>
<script src="{{url('js/select2-custom.js')}}"></script>
<!--This page plugins -->
<script src="{{url('adminmart/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('adminmart/dist/js/pages/datatable/datatable-basic.init.js')}}"></script>
<script src="{{url('adminmart/sweetalert2/sweetalert2/sweetalert2.min.js')}}"></script>
<!--Custom JavaScript -->
<script src="{{url('adminmart/dist/js/custom.min.js')}}"></script>
@stack('scripts')
</body>
</html>

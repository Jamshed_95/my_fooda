@extends('layouts.app')
@section('content')
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-7 align-self-center">
                <h3 class="page-title text-truncate text-dark font-weight-medium mb-1"><i class="fa fa-check-circle"></i> Запросы на регистрицию</h3>
                <div class="d-flex align-items-center">
                    Просмотр и прием
                </div>
            </div>
            <div class="col-5 align-self-center">
                <div class="customize-input float-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb m-0 p-0">
                            <li class="breadcrumb-item"><a href="{{url('index')}}" class="text-primary">Главная</a></li>
                            <li class="breadcrumb-item text-muted active" aria-current="page">Запросы на регистрицию</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <h4 class="card-title mt-2">Данные пользователя</h4>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="dataTable" class="table table-borderless" style="width: 100%">
                        <thead>
                        <tr>
                            <th style="vertical-align: middle; text-align: center">Email</th>
                            <th style="vertical-align: middle; text-align: center">Дата отправки<br><input type="text" class="form-control text-center form-control-sm"></th>
                            <th style="vertical-align: middle; text-align: center">Принят (Роли)</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $('#dataTable').DataTable({
            "ordering": false,
            pageLength : 5,
            lengthMenu: [[5, 10, 50,100, -1], [5, 10, 50, 100, 'Все']],
            "language": {
                "url": "{{ url('js/russian.json') }}"
            },
            ajax: {
                url: '{{ url('getuserZaprosiNaReg/listAjax') }}',
                dataSrc: 'data'
            },
            columns: [
                {data: 'email'},
                {data: 'dataOtpr'},
                {data: 'btn'},
            ],
            "dom": "<'row'<'col-sm-5 pl-3 pt-2 text-muted'i><'col-sm-7 pr-3 pt-3'p>>"
                + "<'row'<'col-sm-12'tr>>"
                + "<'row'<'col-sm-12 text-muted pb-2 text-sm pl-3 pt-3'l>>",
            "scrollX": true,
            "createdRow": function (row) {
                $('td', row).css('vertical-align', 'middle');
                $('td:first-child', row).css('text-align', 'center');
                $('td:first-child+td', row).css('text-align', 'center');
                $('td:last-child', row).css('text-align', 'center');
            },
            initComplete: function () {
                this.api().columns('.select-filter').every(function () {
                    var column = this;
                    var select = $('<select  class="form-control form-control-sm"><option value=""></option></select>')
                        .appendTo($(column.header()))
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                    column.draw();
                });

                this.api().columns().every(function () {
                    var that = this;

                    $('input', this.header()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                .search(this.value)
                                .draw();
                        }
                    });
                });
            }
        });
    </script>
@endpush

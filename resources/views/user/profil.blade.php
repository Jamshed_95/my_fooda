@extends('layouts.app')
@push('scripts')
    <script type="text/javascript">
        function editUser(userId) {
            $.ajax({
                type: "GET",
                cache: false,
                url: '{{url("getUser/")}}/' + userId,
                context: document.body,
            }).done(function (user) {
                view = ' <div class="form-group">\
                            <input type="text" id="userId" name="userId" hidden>\
                            <label for="emailaddress"><i class="icon-envelope-open"></i> Email</label>\
                            <input class="form-control" type="email" id="emailaddress" name="emailaddress" required="" placeholder="john@deo.com">\
                        </div> \
                         <div class="form-group">\
                            <label for="password"><i class="icon-lock"></i> Пароль</label>\
                            <input class="form-control" type="password" id="password" name="password" >\
                        </div>\
                        ';
                $('#viewEdit').html(view);
                $('#userId').val(user.id);
                $('#emailaddress').val(user.email);
                // $('#password').val(user.password);
            });
        }
    </script>
@endpush
@section('content')
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-7 align-self-center">
                <h3 class="page-title text-truncate text-dark font-weight-medium mb-1"><i class="fa fa-user"></i> Мой профиль</h3>
                <div class="d-flex align-items-center">
                    Просмотр данных пользователя
                </div>
            </div>
            <div class="col-5 align-self-center">
                <div class="customize-input float-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb m-0 p-0">
                            <li class="breadcrumb-item"><a href="{{url('index')}}" class="text-primary">Главная</a></li>
                            <li class="breadcrumb-item text-muted active" aria-current="page">Мой профиль</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <h4 class="card-title mt-2">Данные пользователя</h4>
                            </div>
                            <div class="col-lg-6 text-right">
                                <a href="#editUserForm" class="btn btn-outline-primary btn-rounded" onclick="editUser('{{$user->id}}')"
                                   role="button" data-toggle="modal" title="Редактировать"> <i class="icon-pencil"> Редактировать</i>
                                </a>
                            </div>

                            <div id="editUserForm" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <div class="text-center mt-2 mb-4">
                                                Редактирование данных
                                            </div>

                                            <form class="pl-3 pr-3" action="{{url('editUser/save')}}" method="post">
                                                @csrf
                                                <div id="viewEdit"></div>
                                                <div class="form-group text-center">
                                                    <button class="btn btn-primary" type="submit">Сохранить изминения</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mt-4 activity">
                            <div class="d-flex align-items-start border-left-line pb-3">
                                <div>
                                    <a href="javascript:void(0)" class="btn btn-danger btn-circle mb-2 btn-item">
                                        <i class=" icon-envelope-open"></i>
                                    </a>
                                </div>
                                <div class="ml-3 mt-2">
                                    <h5 class="text-dark font-weight-medium mb-2">{{$user->email}}</h5>
                                    <p class="font-14 mb-2 text-muted">Почта</p>
                                </div>
                            </div>
                            <div class="d-flex align-items-start border-left-line pb-3">
                                <div>
                                    <a href="javascript:void(0)"
                                       class="btn btn-success btn-circle mb-2 btn-item">
                                        <i class="icon-clock"></i>
                                    </a>
                                </div>
                                <div class="ml-3 mt-2">
                                    <h5 class="text-dark font-weight-medium mb-2">{{\Carbon\Carbon::parse($user->created_at)->format('d.m.Y в h:i:s')}}</h5>
                                    <p class="font-14 mb-2 text-muted">Дата регистрации</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @if(Session::get('successEdit'))
        <input type="hidden" id="successEdit">
    @elseif(Session::get('notUser'))
        <input type="hidden" id="notUser">
    @endif
@endsection
@push('scripts')
    <script>
        if ($('#successEdit').length > 0) {
            Swal.fire({
                type: 'success',
                title: 'Данные успешно изминены!',
                showConfirmButton: false,
                timer: 2000
            })
        }
        if ($('#notUser').length > 0) {
            Swal.fire({
                type: 'error',
                title: 'Пользователь не найден!',
                showConfirmButton: false,
                timer: 2000
            })
        }
    </script>
@endpush

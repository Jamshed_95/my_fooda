@extends('layouts.app')
@section('content')
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-7 align-self-center">
                <h3 class="page-title text-truncate text-dark font-weight-medium mb-1 "><i class="far fa-file-alt"></i> Заявки на разбор</h3>
                <div class="d-flex align-items-center">
                    Просмотр и действие
                </div>
            </div>
            <div class="col-5 align-self-center">
                <div class="customize-input float-right">
                   ...
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="card">
            <div class="card-body p-0">
                <div class="table-responsive">
                    <table id="dataTable" class="table table-borderless" style="width: 100%">
                        <thead>
                        <tr>
                            <th style="vertical-align: middle; text-align: center">Изображение №1</th>
                            <th style="vertical-align: middle; text-align: center">Изображение №2</th>
                            <th style="vertical-align: middle; text-align: center">Изображение №3</th>
                            <th style="vertical-align: middle; text-align: center">Штрихкод</th>
                            <th style="vertical-align: middle; text-align: center">Комментарий</th>
                            <th style="vertical-align: middle; text-align: center">Принять</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @if(Session::get('prinat'))
        <input type="hidden" id="prinat" name="prinat">
    @endif
@endsection
@push('scripts')
    <script>
        if ($('#prinat').length > 0) {
            Swal.fire({
                type: 'success',
                title: 'Заявка принято на разбор!',
                showConfirmButton: false,
                timer: 2000
            })
        }
        $('#dataTable').DataTable({
            "ordering": false,
            "language": {
                "url": "{{ url('js/russian.json') }}"
            },
            ajax: {
                url: '{{ url('/zayavkiNaRazbor/listAjax') }}',
                dataSrc: 'data'
            },
            columns: [
                {data: 'img1', width: "20%"},
                {data: 'img2', width: "20%"},
                {data: 'img3', width: "20%"},
                {data: 'shtrihkod'},
                {data: 'comment'},
                {data: 'btn'},
            ],
            "dom": "<'row'<'col-sm-5 pl-3 pt-2 text-muted'i><'col-sm-7 pr-3 pt-3'p>>"
                + "<'row'<'col-sm-12'tr>>"
                + "<'row'<'col-sm-12 text-muted pb-2 text-sm pl-3 pt-3'l>>",
            "scrollX": true,
            "createdRow": function (row) {
                $('td', row).css('vertical-align', 'middle');
                $('td:first-child', row).css('text-align', 'center');
                $('td:first-child+td', row).css('text-align', 'center');
                $('td:first-child+td+td', row).css('text-align', 'center');
                $('td:last-child', row).css('text-align', 'center');
            },
            initComplete: function () {
                this.api().columns('.select-filter').every(function () {
                    var column = this;
                    var select = $('<select  class="form-control form-control-sm"><option value=""></option></select>')
                        .appendTo($(column.header()))
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                    column.draw();
                });

                this.api().columns().every(function () {
                    var that = this;

                    $('input', this.header()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                .search(this.value)
                                .draw();
                        }
                    });
                });
            }
        });

    </script>
@endpush

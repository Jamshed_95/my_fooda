@extends('layouts.app')
@push('scripts')
    <script type="text/javascript">
        function editProduct(productId) {
            $.ajax({
                type: "GET",
                cache: false,
                url: '{{url("getProduct/")}}/' + productId,
                context: document.body,
            }).done(function (product) {
                if (product.imgProduct==null){
                    var imgHas='изображение не загружено';
                    var imgTextColor='danger';
                }else {
                    var imgHas='изображение загружено';
                    var imgTextColor='success';
                }
                view = '\n' +
                    ' <input id="idProd" name="idProduct" hidden>'+
                    '                    <div class="modal-body"  style=" max-height: calc(100vh - 210px); overflow-y: auto;">\n' +
                    '                        <table class="tb_td table m-0 table-borderless" >\n' +
                    '                            <tr>\n' +
                    '                                <td>\n' +
                    '                                    <div class="form-group mb-2">\n' +
                    '                                        <label for="nameProduct"><i class="fas fa-quote-left text-primary"></i> Название</label>\n' +
                    '                                        <input class="form-control" type="text" id="nameProd" name="nameProduct" >\n' +
                    '                                    </div>\n' +
                    '                                </td>\n' +
                    '                                <td>\n' +
                    '                                    <div class="form-group mb-2">\n' +
                    '                                        <label for="categoryId"><i class="fas fa-tasks text-primary"></i> Категория</label>\n' +
                    '                                        <select class="form-control select2" name="categoryId" id="catId" >\n' +
                    '                                            <option value="" selected disabled>Не выбрано</option>\n' +
                    '                                            @foreach($category as $cat)\n' +
                    '                                                <option value="{{$cat->id}}">{{$cat->name}}</option>\n' +
                    '                                            @endforeach\n' +
                    '                                        </select>\n' +
                    '                                    </div>\n' +
                    '                                </td>\n' +
                    '                            </tr>\n' +
                    '                            <tr>\n' +
                    '                                <td colspan="2">\n' +
                    '                                    <div class="form-group mb-2">\n' +
                    '                                        <label for="dopOpisanie"><i class="fas fa-indent text-primary"></i> Дополнительное описание</label>\n' +
                    '                                        <textarea rows="1" class="form-control" type="text" id="dopOpis" name="dopOpisanie" ></textarea>\n' +
                    '                                    </div>\n' +
                    '                                </td>\n' +
                    '                            </tr>\n' +
                    '                            <tr>\n' +
                    '                                <td>\n' +
                    '                                    <div class="form-group mb-2">\n' +
                    '                                        <label for="imgProduct"><i class="fas fa-images text-primary"></i> Изображение <small class="text-'+imgTextColor+'">('+imgHas+')</small></label>\n' +
                    '                                        <div class="input-group">\n' +
                    '                                            <div class="custom-file">\n' +
                    '                                                <input type="file" class="custom-file-input" name="imgProduct" accept=".png,.jpg,.ico">\n' +
                    '                                                <label class="custom-file-label typeFile" for="inputGroupFile04"></label>\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                </td>\n' +
                    '                                <td colspan="2">\n' +
                    '                                    <div class="form-group mb-2">\n' +
                    '                                        <label for="proizvoditel"><i class="fas fa-truck-moving text-primary"></i> Производитель</label>\n' +
                    '                                        <textarea rows="1" class="form-control" type="text" id="proiz" name="proizvoditel" ></textarea>\n' +
                    '                                    </div>\n' +
                    '                                </td>\n' +
                    '                            </tr>\n' +
                    '                            <tr>\n' +
                    '                                <td colspan="2">\n' +
                    '                                    <div class="form-group mb-2">\n' +
                    '                                        <label for="sostav"><i class="fas fa-recycle text-warning"></i> Состав</label>\n' +
                    '                                        <textarea rows="5" class="form-control" type="text" id="sost" name="sostav" ></textarea>\n' +
                    '                                    </div>\n' +
                    '                                </td>\n' +
                    '                            </tr>\n' +
                    '                            <tr>\n' +
                    '                                <td colspan="2">\n' +
                    '                                    <div class="form-group mb-2">\n' +
                    '                                        <label for="nezhelDobavki"><i class="fas fa-exclamation-triangle text-warning"></i> Нежелательные или опасные добавки</label>\n' +
                    '                                        <textarea rows="2" class="form-control" type="text" id="nezhelDob" name="nezhelDobavki" ></textarea>\n' +
                    '                                    </div>\n' +
                    '                                </td>\n' +
                    '                            </tr>\n' +
                    '                        </table>\n' +
                    '                        <table class="tb_td table m-0 table-borderless" style="width: 100%">\n' +
                    '                            <tr>\n' +
                    '                                <td>\n' +
                    '                                    <div class="form-group mb-2">\n' +
                    '                                        <label for="kkal"><i class="fas fa-tint text-cyan"></i> Ккал на 100гр.</label>\n' +
                    '                                        <input class="form-control" type="text" id="kkall" name="kkal" \n' +
                    '                                                onkeypress=\'return (event.charCode >= 42 && event.charCode <= 57)\'>\n' +
                    '                                    </div>\n' +
                    '                                </td>\n' +
                    '                                <td>\n' +
                    '                                    <div class="form-group mb-2">\n' +
                    '                                        <label for="belki"><i class="fas fa-tint text-cyan"></i> Белки на 100гр.</label>\n' +
                    '                                        <input class="form-control" type="text" id="belkiEdit" name="belki" \n' +
                    '                                                onkeypress=\'return (event.charCode >= 42 && event.charCode <= 57)\'>\n' +
                    '                                    </div>\n' +
                    '                                </td>\n' +
                    '                                <td>\n' +
                    '                                    <div class="form-group mb-2">\n' +
                    '                                        <label for="zhir"><i class="fas fa-tint text-cyan"></i> Жиры на 100гр.</label>\n' +
                    '                                        <input class="form-control" type="text" id="zhirEdit" name="zhir"\n' +
                    '                                                onkeypress=\'return (event.charCode >= 42 && event.charCode <= 57)\'>\n' +
                    '                                    </div>\n' +
                    '                                </td>\n' +
                    '                            </tr>\n' +
                    '                            <tr>\n' +
                    '                                <td>\n' +
                    '                                    <div class="form-group mb-2">\n' +
                    '                                        <label for="uglevodi"><i class="fas fa-tint text-cyan"></i> Углеводы на 100гр.</label>\n' +
                    '                                        <input class="form-control" type="text" id="uglevodiEdit" name="uglevodi" \n' +
                    '                                                onkeypress=\'return (event.charCode >= 42 && event.charCode <= 57)\'>\n' +
                    '                                    </div>\n' +
                    '                                </td>\n' +
                    '                                <td>\n' +
                    '                                    <div class="form-group mb-2">\n' +
                    '                                        <label for="sahar"><i class="fas fa-tint text-cyan"></i> Сахар на 100гр.</label>\n' +
                    '                                        <input class="form-control" type="text" id="saharEdit" name="sahar"\n' +
                    '                                                onkeypress=\'return (event.charCode >= 42 && event.charCode <= 57)\'>\n' +
                    '                                    </div>\n' +
                    '                                </td>\n' +
                    '                                <td>\n' +
                    '                                    <div class="form-group mb-2">\n' +
                    '                                        <label for="kletchatka"><i class="fas fa-tint text-cyan"></i> Клетчатка на 100гр.</label>\n' +
                    '                                        <input class="form-control" type="text" id="kletchatkaEdit" name="kletchatka" \n' +
                    '                                                onkeypress=\'return (event.charCode >= 42 && event.charCode <= 57)\'>\n' +
                    '                                    </div>\n' +
                    '                                </td>\n' +
                    '                            </tr>\n' +
                    '                            <tr>\n' +
                    '                                <td colspan="2">\n' +
                    '                                    <div class="form-group mb-2">\n' +
                    '                                        <label for="shtrihkod"><i class="fas fa-barcode text-secondary"></i> Штрихкод</label>\n' +
                    '                                        <input class="form-control" type="number" id="shtrihkodEdit" name="shtrihkod" \n' +
                    '                                               >\n' +
                    '                                    </div>\n' +
                    '                                </td>\n' +
                    '                                <td>\n' +
                    '                                    <div class="form-group mb-2">\n' +
                    '                                        <label for="gdeKupit"><i class="fas fa-cart-plus text-primary"></i> Где купить</label>\n' +
                    '                                        <select class="select2" multiple="multiple" data-placeholder="Где купить" style="width: 100%;" name="gdeKupit[]" id="gdeKupitEdit">' +
                    '                                            @foreach($gdeKupitSp as $gdeKupit)\n' +
                    '                                                <option value="{{$gdeKupit->id}}">{{$gdeKupit->name}}</option>\n' +
                    '                                            @endforeach\n' +
                    '                                        </select>\n' +
                    '                                    </div>\n' +
                    '                                </td>\n' +
                    '                            </tr>\n' +
                    '                            <tr>\n' +
                    '                                <td>\n' +
                    '                                    <div class="form-group mb-2">\n' +
                    '                                        <label for="ikKachestva"><i class="fas fa-check-circle text-success"></i> Иконка качества</label>\n' +
                    '                                        <select class="select2" multiple="multiple" data-placeholder="Качество" style="width: 100%;" name="ikKachestva[]" id="ikKachestvaEdit">\n' +
                    '                                            @foreach($ikonkaKachestva as $ikonkaKachest)\n' +
                    '                                                <option value="{{$ikonkaKachest->id}}"> {{$ikonkaKachest->name}}</option>\n' +
                    '                                            @endforeach\n' +
                    '                                        </select>\n' +
                    '                                    </div>\n' +
                    '                                </td>\n' +
                    '                                <td>\n' +
                    '                                    <div class="form-group mb-2">\n' +
                    '                                        <label for="vivod"><i class="fas fa-bullhorn text-danger"></i> Вывод</label>\n' +
                    '                                        <select class="form-control" name="vivod" id="vivodEdit" >\n' +
                    '                                            <option value="" selected disabled>Не выбрано</option>\n' +
                    '                                            @foreach($vivod as $viv)\n' +
                    '                                                <option value="{{$viv->id}}" >{{$viv->name}}</option>\n' +
                    '                                            @endforeach\n' +
                    '                                        </select>\n' +
                    '                                    </div>\n' +
                    '                                </td>\n' +
                    '                                <td>\n' +
                    '                                    <div class="form-group mb-2">\n' +
                    '                                        <label for="statuS"><i class="fas fa-info-circle text-info"></i> Статус</label>\n' +
                    '                                        <select class="form-control" name="statuS" id="statuSEdit" >\n' +
                    '                                            @foreach($statuS as $stat)\n' +
                    '                                                <option value="{{$stat->id}}" >{{$stat->name}}</option>\n' +
                    '                                            @endforeach\n' +
                    '                                        </select>\n' +
                    '                                    </div>\n' +
                    '                                </td>\n' +
                    '                            </tr>\n' +
                    '                        </table>\n' +
                    '                    </div>';
                $('#viewE').html(view);
                $('#idProd').val(product.id);
                $('#nameProd').val(product.nameProduct);
                $('#catId').val(product.category_id);
                $('#dopOpis').val(product.dopOpisanie);
                $('#proiz').val(product.proizvoditel);
                $('#sost').val(product.sostav);
                $('#nezhelDob').val(product.nezhelDobavki);
                $('#kkall').val(product.kkal);
                $('#belkiEdit').val(product.belki);
                $('#zhirEdit').val(product.zhir);
                $('#uglevodiEdit').val(product.uglevodi);
                $('#saharEdit').val(product.sahar);
                $('#kletchatkaEdit').val(product.kletchatka);
                $('#shtrihkodEdit').val(product.shtrihkod);
                $('#gdeKupitEdit').val(product.gde_kupit);
                $('#vivodEdit').val(product.vivod_id);
                $('#statuSEdit').val(product.status_id);
                $('#imgProduct2').val(product.imgProduct);
                $('#ikKachestvaEdit').val(product.kachestvo);

                $(document).ready(function() {
                    var ikKachestv = null;
                    $('#ikKachestvaEdit').change(function(event) {
                        if ($(this).val().length > 3) {
                            $(this).val(ikKachestv);
                        } else {
                            ikKachestv = $(this).val();
                        }
                    });
                });

                $(function () {
                    //Initialize Select2 Elements
                    $('.select2').select2()

                    //Initialize Select2 Elements
                    $('.select2bs4').select2({
                        theme: 'bootstrap4'
                    })
                });
                $(function(){"use strict";feather.replace(),$(".preloader").fadeOut(),$(".nav-toggler").on("click",function(){$("#main-wrapper").toggleClass("show-sidebar"),$(".nav-toggler i").toggleClass("ti-menu")}),$(function(){$(".service-panel-toggle").on("click",function(){$(".customizer").toggleClass("show-service-panel")}),$(".page-wrapper").on("click",function(){$(".customizer").removeClass("show-service-panel")})}),$(function(){$('[data-toggle="tooltip"]').tooltip()}),$(function(){$('[data-toggle="popover"]').popover()}),$(".message-center, .customizer-body, .scrollable, .scroll-sidebar").perfectScrollbar({wheelPropagation:!0}),$("body, .page-wrapper").trigger("resize"),$(".page-wrapper").delay(20).show(),$(".list-task li label").click(function(){$(this).toggleClass("task-done")}),$(".show-left-part").on("click",function(){$(".left-part").toggleClass("show-panel"),$(".show-left-part").toggleClass("ti-menu")}),$(".custom-file-input").on("change",function(){var e=$(this).val();$(this).next(".custom-file-label").html(e)})});
            });
        }
    </script>
@endpush
@push('style_after')
    <style>
        .tb_td td{
            padding: 3px 10px;
        }
        .sidebar-nav {
            width: 0px;
            overflow:hidden;

            -webkit-transition: width 0.3s;
            -moz-transition: width 0.3s;
            -ms-transition: width 0.3s;
            -o-transition: width 0.3s;
            transition: width 0.3s;
        }
        .sidebar-nav.open {
            width: 350px;
        }
        .sidebarnav .has-arrow::after {
            position: revert;
            margin-left: 210px;
        }
        .sidebar-nav .has-arrow::after {
            top: 10px;
            width: 5px;
            height: 5px;
        }
        .sidebar-nav .has-arrow1::after {
            position: absolute;
            content: '';
            width: 7px;
            height: 7px;
            border-width: 1px 0 0 1px;
            border-style: solid;
            border-color: #fff;
            margin-left: 10px;
            -webkit-transform: rotate( 223deg) translate(0,-50%);
            -ms-transform: rotate(223deg) translate(0,-50%);
            -o-transform: rotate(223deg) translate(0,-50%);
            transform: rotate( 223deg) translate(0,-50%);
            -webkit-transform-origin: top;
            -ms-transform-origin: top;
            -o-transform-origin: top;
             transform-origin: top;
             top: 18px;
             right: 15px;
            -webkit-transition: all .3s ease-out;
            -o-transition: all .3s ease-out;
            transition: all .3s ease-out;
        }
        .typeFile {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }
</style>
@endpush
@section('content')
    <div class="page-breadcrumb pt-2 pr-1">
        <div class="row">
            <div class="col-4 align-self-center">
                <h3 class="page-title text-truncate text-dark font-weight-medium mb-1 "><i class="icon-grid"></i> Продукты </h3>
                <div class="d-flex align-items-center">
                    <button class="show btn btn-outline-primary" onclick="hideShowFunction()"> <i class="ti-eye"></i> Показать каталог</button>
                </div>

            </div>
            <div class="col-8 align-self-center">
                <div class="customize-input float-right">
                    <button type="button" class="btn btn-outline-success btn-rounded" data-toggle="modal" data-target="#addProduct">
                        <i class="fas fa-plus-circle"></i> Добавить продукт
                    </button>
                    <div id="addProduct" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" >
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content" style="border-radius: 10px">
                                <form action="{{url('/addNewProduct/save')}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="modal-header">
                                        <h4 class="modal-title"><i class="icon-grid text-danger"></i> Добавить новый продукт</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <div class="modal-body"  style=" max-height: calc(100vh - 210px); overflow-y: auto;">
                                        <input hidden name="type" value="newAdd">
                                        <table class="tb_td table m-0 table-borderless" >
                                            <tr>
                                                <td>
                                                    <div class="form-group mb-2">
                                                        <label for="nameProduct"><i class="fas fa-quote-left text-primary"></i> Название</label>
                                                        <input class="form-control" type="text" id="nameProduct" value="{{old('nameProduct')}}" name="nameProduct" >
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group mb-2">
                                                        <label for="categoryId"><i class="fas fa-tasks text-primary"></i> Категория</label>
                                                        <select class="form-control select2" name="categoryId" id="categoryId" >
                                                            <option value="" selected disabled>Не выбрано</option>
                                                            @foreach($category as $cat)
                                                                <option value="{{$cat->id}}" {{$cat->id==old('categoryId')?'selected':''}}>{{$cat->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class="form-group mb-2">
                                                        <label for="dopOpisanie"><i class="fas fa-indent text-primary"></i> Дополнительное описание</label>
                                                        <textarea rows="1" class="form-control" type="text" id="dopOpisanie" name="dopOpisanie" >{{old('dopOpisanie')}}</textarea>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="form-group mb-2">
                                                        <label for="imgProduct"><i class="fas fa-images text-primary"></i> Изображение <small style="font-size: 14px" id="textImagee"></small> </label>
                                                        <div class="input-group">
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input" onchange="textImageFiled()" name="imgProduct" id="imgProduct" accept=".png,.jpg,.ico" >
                                                                <label class="custom-file-label typeFile" for="inputGroupFile04"></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td colspan="2">
                                                    <div class="form-group mb-2">
                                                        <label for="proizvoditel"><i class="fas fa-truck-moving text-primary"></i> Производитель</label>
                                                        <textarea rows="1" class="form-control" type="text" id="proizvoditel" name="proizvoditel" >{{old('proizvoditel')}}</textarea>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class="form-group mb-2">
                                                        <label for="sostav"><i class="fas fa-recycle text-warning"></i> Состав</label>
                                                        <textarea rows="5" class="form-control" type="text" id="sostav" name="sostav" >{{old('sostav')}}</textarea>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class="form-group mb-2">
                                                        <label for="nezhelDobavki"><i class="fas fa-exclamation-triangle text-warning"></i> Нежелательные или опасные добавки</label>
                                                        <textarea rows="2" class="form-control" type="text" id="nezhelDobavki" name="nezhelDobavki" >{{old('nezhelDobavki')}}</textarea>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="tb_td table m-0 table-borderless" style="width: 100%">
                                            <tr>
                                                <td>
                                                    <div class="form-group mb-2">
                                                        <label for="kkal"><i class="fas fa-tint text-cyan"></i> Ккал на 100гр.</label>
                                                        <input class="form-control" type="text" id="kkal" name="kkal" value="{{old('kkal')}}"
                                                               onkeypress='return (event.charCode >= 42 && event.charCode <= 57)'>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group mb-2">
                                                        <label for="belki"><i class="fas fa-tint text-cyan"></i> Белки на 100гр.</label>
                                                        <input class="form-control" type="text" id="belki" name="belki" value="{{old('belki')}}"
                                                               onkeypress='return (event.charCode >= 42 && event.charCode <= 57)'>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group mb-2">
                                                        <label for="zhir"><i class="fas fa-tint text-cyan"></i> Жиры на 100гр.</label>
                                                        <input class="form-control" type="text" id="zhir" name="zhir" value="{{old('zhir')}}"
                                                               onkeypress='return (event.charCode >= 42 && event.charCode <= 57)'>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="form-group mb-2">
                                                        <label for="uglevodi"><i class="fas fa-tint text-cyan"></i> Углеводы на 100гр.</label>
                                                        <input class="form-control" type="text" id="uglevodi" name="uglevodi" value="{{old('uglevodi')}}"
                                                               onkeypress='return (event.charCode >= 42 && event.charCode <= 57)'>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group mb-2">
                                                        <label for="sahar"><i class="fas fa-tint text-cyan"></i> Сахар на 100гр.</label>
                                                        <input class="form-control" type="text" id="sahar" name="sahar" value="{{old('sahar')}}"
                                                               onkeypress='return (event.charCode >= 42 && event.charCode <= 57)'>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group mb-2">
                                                        <label for="kletchatka"><i class="fas fa-tint text-cyan"></i> Клетчатка на 100гр.</label>
                                                        <input class="form-control" type="text" id="kletchatka" name="kletchatka" value="{{old('kletchatka')}}"
                                                               onkeypress='return (event.charCode >= 42 && event.charCode <= 57)'>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class="form-group mb-2">
                                                        <label for="shtrihkod"><i class="fas fa-barcode text-secondary"></i> Штрихкод</label>
                                                        <input class="form-control" type="number" id="shtrihkod" name="shtrihkod" value="{{old('shtrihkod')}}"
                                                        >
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group mb-2">
                                                        <label for="gdeKupit"><i class="fas fa-cart-plus text-primary"></i> Где купить</label>
                                                        <select class="select2" multiple="multiple" data-placeholder="Select" style="width: 100%;" name="gdeKupit[]" id="gdeKupit">
                                                            @foreach($gdeKupitSp as $gdeKupit)
                                                                <option value="{{$gdeKupit->id}}" {{$gdeKupit->id==old('gdeKupit[]')?'selected':''}}> {{$gdeKupit->name}}</option>
                                                            @endforeach
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="form-group mb-2">
                                                        <label for="ikKachestva"><i class="fas fa-check-circle text-success"></i> Иконка качества</label>
                                                        <select class="select2" multiple="multiple" data-placeholder="Select" style="width: 100%;" name="ikKachestva[]" id="ikKachestva">
                                                            @foreach($ikonkaKachestva as $ikonkaKachest)
                                                                <option value="{{$ikonkaKachest->id}}" {{$ikonkaKachest->id==old('ikKachestva[]')?'selected':''}}> {{$ikonkaKachest->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group mb-2">
                                                        <label for="vivod"><i class="fas fa-bullhorn text-danger"></i> Вывод</label>
                                                        <select class="form-control" name="vivod" id="vivod" >
                                                            <option value="" selected disabled>Не выбрано</option>
                                                            @foreach($vivod as $viv)
                                                                <option value="{{$viv->id}}" {{$viv->id==old('vivod')?'selected':''}}>{{$viv->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group mb-2">
                                                        <label for="statuS"><i class="fas fa-info-circle text-info"></i> Статус</label>
                                                        <select class="form-control" name="statuS" id="statuS" >
                                                            @foreach($statuS as $stat)
                                                                <option value="{{$stat->id}}" {{$stat->id==15?'selected':''}}>{{$stat->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-outline-secondary btn-rounded mt-3 float-left" data-dismiss="modal"><i class="fa fa-times"></i> Отменить</button>
                                        <button type="submit" class="btn btn-outline-success btn-rounded mt-3 float-right"><i class="fa fa-save"></i> Сохранить</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-lg-8">
                <a href="{{url('/product/all')}}" class="btn btn-outline-dark {{$status=='all'?'active':''}}">
                    <i class="fas fa-list-alt text-primary"></i> Все <small>({{$infoStatus[0]->countt+$infoStatus[1]->countt+$infoStatus[2]->countt+$infoStatus[3]->countt}})</small>
                </a>
                <a href="{{url('/product/14')}}" class="btn btn-outline-dark {{$status=='14'?'active':''}}">
                    <i class="fas fa-spinner text-info"></i> На утверждении <small>({{$infoStatus[1]->countt}})</small>
                </a>
                <a href="{{url('/product/13')}}" class="btn btn-outline-dark {{$status=='13'?'active':''}}">
                    <i class="fas fa-check text-success"></i> Опубликованные  <small>({{$infoStatus[0]->countt}})</small>
                </a>
                <a href="{{url('/product/15')}}" class="btn btn-outline-dark {{$status=='15'?'active':''}}">
                    <i class="fas  fa-file text-warning"></i> Черновики <small>({{$infoStatus[2]->countt}})</small>
                </a>
                <a href="{{url('/product/16')}}" class="btn btn-outline-dark {{$status=='16'?'active':''}}">
                    <i class="fas fa-trash text-danger"></i> Корзина  <small>({{$infoStatus[3]->countt}})</small>
                </a>
            </div>
        </div>
    </div>

    <div class="container-fluid pt-2 pr-1">
        <div class="row justify-content-center">
            <nav class="sidebar-nav pt-0 mb-3">
                <div class="col-lg-3 pr-0 pl-0" style="max-width: 100%;">
                    <div class="bg-primary" style="border-radius: 5px">
                        <ul id="sidebarnav">
                            <li class="sidebar-item" style="color: black">
                                <a class="sidebar-link p-2 has-arrow1" href="javascript:void(0)" aria-expanded="false">
                                    <i data-feather="tag" class="feather-icon"></i> <span class="hide-menu">Продукты</span>
                                </a>
                                <?=$allCategories_product?>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="col-lg-12 pr-1" id="contents">
                <div class="card">
                    <div class="card-body p-1">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-borderless table-striped" style="width: 100%">
                                <thead>
                                <tr>
                                    <th style="vertical-align: middle; text-align: center">Название<br><input type="text" class="form-control  form-control-sm"></th>
                                    <th style="vertical-align: middle; text-align: center">Категория<br><input type="text" class="form-control  form-control-sm"></th>
                                    <th style="vertical-align: middle; text-align: center">Производитель<input type="text" class="form-control  form-control-sm"></th>
                                    <th style="vertical-align: middle; text-align: center">Состав<input type="text" class="form-control  form-control-sm"></th>
                                    <th style="vertical-align: middle; text-align: center">Сахар<input type="text" class="form-control  form-control-sm text-center"></th>
                                    <th style="vertical-align: middle; text-align: center">Вывод<input type="text" class="form-control form-control-sm "></th>
                                    <th style="vertical-align: middle; text-align: center">Статус<input type="text" class="form-control  form-control-sm "></th>
                                    <th style="vertical-align: middle; text-align: center">Действие</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="editForm" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="border-radius: 10px">
                <form action="{{url('/addNewProduct/save')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h4 class="modal-title"><i class="icon-grid text-danger"></i> Редактирование продукта</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <input hidden name="type" value="edit">
                    <div id="viewE"></div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-outline-secondary btn-rounded mt-3 float-left" data-dismiss="modal"><i class="fa fa-times"></i> Отменить</button>
                        <button type="submit" class="btn btn-outline-success btn-rounded mt-3 float-right"><i class="fa fa-save"></i> Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $('#dataTable').DataTable({
            "ordering": false,
            pagingType: 'full_numbers',
            pageLength : 50,
            lengthMenu: [[50, 100, 200, 300, -1], [50, 100, 200, 300, 'Все']],
            "language": {
                "url": "{{ url('js/russian.json') }}"
            },
            ajax: {
                url: '{{ url('/product/listAjax/'.$status) }}',
                dataSrc: 'data'
            },
            columns: [
                {data: 'nameProduct', width: "20%"},
                {data: 'categoryName'},
                {data: 'proizvoditel'},
                {data: 'sostav'},
                {data: 'sahar'},
                {data: 'vivod'},
                {data: 'statusName'},
                {data: 'btn'}
            ],
            "dom": "<'row'<'col-sm-5 pl-3 pt-2 text-muted'i><'col-sm-7 pr-3 pt-3'p>>"
                + "<'row'<'col-sm-12'tr>>"
                + "<'row'<'col-sm-12 text-muted pb-2 text-sm pl-3 pt-3'l>>",
            "scrollX": true,
            "createdRow": function (row) {
                $('td', row).css('vertical-align', 'middle');
                $('td', row).css('text-align', 'center');
                $('td:first-child+td+td+td', row).css('text-align', 'left');
            },
            initComplete: function () {
                this.api().columns('.select-filter').every(function () {
                    var column = this;
                    var select = $('<select  class="form-control form-control-sm"><option value=""></option></select>')
                        .appendTo($(column.header()))
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                    column.draw();
                });

                this.api().columns().every(function () {
                    var that = this;

                    $('input', this.header()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                .search(this.value)
                                .draw();
                        }
                    });
                });
            }
        });

        $(document).ready(function() {
            var ikKachestv = null;
            $('#ikKachestva').change(function(event) {
                if ($(this).val().length > 3) {
                    $(this).val(ikKachestv);
                } else {
                    ikKachestv = $(this).val();
                }
            });
            var imgT = document.getElementById('imgProduct').value;
            var span = document.getElementById('textImagee');
            if(imgT==null || imgT==''){
                span.textContent = '(Изображение еще не выбрано!)';
                span.className ='text-danger';
            }else{
                span.textContent = '(Изображение выбрано)';
                span.className ='text-success';
            }
        });

        $(function () {
            $('.select2').select2()
        });

        var $yourSidebar = $(".sidebar-nav");
        var $yourSidebar1 =  $(".col-lg-9");

        function hideShowFunction() {
            $yourSidebar.toggleClass("open");
            if ($("#contents").hasClass("col-lg-12")) {
                $('#contents').removeClass('col-lg-12').addClass('col-lg-9');
            }else {
                $('#contents').removeClass('col-lg-9').addClass('col-lg-12');
            }
        }
        function textImageFiled(){
            var imgT = document.getElementById('imgProduct').value;
            var span = document.getElementById('textImagee');
            if(imgT==null || imgT==''){
                span.textContent = '(Изображение еще не выбрано!)';
                span.className ='text-danger';
            }else{
                span.textContent = '(Изображение выбрано)';
                span.className ='text-success';
            }
        }
    </script>
@endpush

@extends('layouts.app')
@push('scripts')
    <script type="text/javascript">
        function editCategory(categoryId) {
            $.ajax({
                type: "GET",
                cache: false,
                url: '{{url("getCategory/")}}/' + categoryId,
                context: document.body,
            }).done(function (category) {
                view = '<div class="form-group">\
                            <label for="username"><i class="icon-tag"> </i>Название</label>\
                            <input class="form-control" type="text" id="nameCat" name="nameCategory" required>\
                            <input type="text" id="categoryId" name="categoryId" hidden>\
                        </div>\
                        <div class="form-group">\
                            <label for="username"><i class="icon-picture"> </i>Изображение</label>\
                            <div class="input-group">\
                                <div class="custom-file">\
                                    <input type="file" class="custom-file-input" name="imgCategry" accept=".png,.jpg,.ico">\
                                    <label class="custom-file-label" for="inputGroupFile04">'+category.img_name+'</label>\
                                </div> \
                            </div>';
                $('#viewE').html(view);
                $('#categoryId').val(category.id);
                $('#nameCat').val(category.name);
            });
        }
        function onClickDelete(e){
            var answer = window.confirm("Удалить каталог?");
            if (answer) {
                return true;
            }
            else {
                e.preventDefault();
                return false;
            }
        };

    </script>

    @push('style_after')
        <style>
            .sidebar-nav {
                width: 0px;
                overflow:hidden;

                -webkit-transition: width 0.3s;
                -moz-transition: width 0.3s;
                -ms-transition: width 0.3s;
                -o-transition: width 0.3s;
                transition: width 0.3s;
            }
            .sidebar-nav.open {
                width: 350px;
            }
            .sidebarnav .has-arrow::after {
                position: revert;
                margin-left: 210px;
            }
            .sidebar-nav .has-arrow::after {
                top: 10px;
                width: 5px;
                height: 5px;
            }
            .sidebar-nav .has-arrow1::after {
                position: absolute;
                content: '';
                width: 7px;
                height: 7px;
                border-width: 1px 0 0 1px;
                border-style: solid;
                border-color: #fff;
                margin-left: 10px;
                -webkit-transform: rotate( 223deg) translate(0,-50%);
                -ms-transform: rotate(223deg) translate(0,-50%);
                -o-transform: rotate(223deg) translate(0,-50%);
                transform: rotate( 223deg) translate(0,-50%);
                -webkit-transform-origin: top;
                -ms-transform-origin: top;
                -o-transform-origin: top;
                transform-origin: top;
                top: 18px;
                right: 15px;
                -webkit-transition: all .3s ease-out;
                -o-transition: all .3s ease-out;
                transition: all .3s ease-out;
            }
        </style>
    @endpush
@endpush
@section('content')
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-7 align-self-center">
                <h3 class="page-title text-truncate text-dark font-weight-medium mb-1 "><i class="icon-layers"></i> Категории</h3>
                <div class="d-flex align-items-center">
                    <button class="show btn btn-outline-primary" onclick="hideShowFunction()"> <i class="ti-eye"></i> Показать каталог</button>
                </div>
            </div>
            <div class="col-5 align-self-center">
                <div class="customize-input float-right">
                    <button type="button" class="btn btn-outline-primary btn-rounded" data-toggle="modal" data-target="#addCategory"><i class="fas fa-plus-circle"> Добавить категорию</i></button>
                    <div id="addCategory" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <h3 class="card-title">
                                        <i class="icon-star text-danger"> Добавить новую категорию</i>
                                    </h3><br>
                                    <form class="pl-3 pr-3" action="{{url('category/save')}}" method="post" enctype="multipart/form-data">
                                        <input hidden name="type" value="addNew">
                                        @csrf
                                        <div class="form-group mb-2">
                                            <label for="category"><i class="fas fa-tasks text-primary"></i> Категория</label>
                                            <select class="form-control select2" name="category" id="category">
                                                <option value="" selected disabled>Не выбрано</option>
                                                @foreach($category as $cat)
                                                    <option value="{{$cat->id}}" {{$cat->id==old('category')?'selected':''}}>{{$cat->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nameCategory"><i class="icon-tag"> </i> Название</label>
                                            <input class="form-control" type="text" id="nameCategory" value="{{old('nameCategory')}}" name="nameCategory" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="imgCategry"><i class="icon-picture"> </i> Изображение</label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" name="imgCategry" accept=".png,.jpg,.ico">
                                                    <label class="custom-file-label" for="inputGroupFile04"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group text-center">
                                            <button class="btn btn-outline-success btn-rounded mt-3" type="submit"><i class="fa fa-save"> </i> Добавить категорию</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid pt-2">
        <div class="row justify-content-center">
            <nav class="sidebar-nav pt-0 mb-3">
                <div class="col-lg-3 pr-0" style="max-width: 100%;">
                    <div class="bg-primary" style="border-radius: 5px">
                        <ul id="sidebarnav">
                            <li class="sidebar-item" style="color: black">
                                <a class="sidebar-link p-2 has-arrow1" href="javascript:void(0)" aria-expanded="false">
                                    <i data-feather="tag" class="feather-icon"></i> <span class="hide-menu">Категории</span>
                                </a>
                                <?=$allCategories?>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="col-lg-9 pl-1">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-borderless" style="width: 100%">
                                <thead>
                                <tr>
                                    <th style="vertical-align: middle; text-align: center">Изображение</th>
                                    <th style="vertical-align: middle; text-align: center">Название<br><input type="text" class="form-control text-center form-control-sm"></th>
                                    <th style="vertical-align: middle; text-align: center">Родители<br><input type="text" class="form-control text-center form-control-sm"></th>
                                    @if($superAdmin)
                                        <th style="vertical-align: middle; text-align: center">Удалить</th>
                                    @endif
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div id="editCategory" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <h3 class="card-title">
                        <i class="icon-pencil text-warning"></i> Редактирование категории
                    </h3><br>
                    <form class="pl-3 pr-3" action="{{url('category/save')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input hidden name="type" value="edit">
                        <div id="viewE"></div>
                        <div class="form-group text-center">
                            <button class="btn btn-outline-success btn-rounded mt-3" type="submit"><i class="fa fa-save"> Изменить</i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        });
        var $yourSidebar = $(".sidebar-nav");
        function hideShowFunction() {
            $yourSidebar.toggleClass("open");
        }
        $('#dataTable').DataTable({
            "ordering": false,
            pageLength : 50,
            lengthMenu: [[50, 100, 200, 300, -1], [50, 100, 200, 300, 'Все']],
            "language": {
                "url": "{{ url('js/russian.json') }}"
            },
            ajax: {
                url: '{{ url('category/listAjax/'.$cat_id) }}',
                dataSrc: 'data'
            },
            columns: [
                {data: 'img', width: "20%"},
                {data: 'nameCategory', width: "20%"},
                {data: 'potomki'},
                @if($superAdmin)
                    {data: 'btn', width: "5%"},
                @endif
            ],
            "dom": "<'row'<'col-sm-5 pl-3 pt-2 text-muted'i><'col-sm-7 pr-3 pt-3'p>>"
                + "<'row'<'col-sm-12'tr>>"
                + "<'row'<'col-sm-12 text-muted pb-2 text-sm pl-3 pt-3'l>>",
            "scrollX": true,
            "createdRow": function (row) {
                $('td', row).css('vertical-align', 'middle');
                $('td:first-child', row).css('text-align', 'center');
                $('td:first-child+td', row).css('text-align', 'center');
                $('td:first-child+td+td', row).css('text-align', 'center');
                $('td:last-child', row).css('text-align', 'center');
            },
            initComplete: function () {
                this.api().columns('.select-filter').every(function () {
                    var column = this;
                    var select = $('<select  class="form-control form-control-sm"><option value=""></option></select>')
                        .appendTo($(column.header()))
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                    column.draw();
                });

                this.api().columns().every(function () {
                    var that = this;

                    $('input', this.header()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                .search(this.value)
                                .draw();
                        }
                    });
                });
            }
        });
    </script>
@endpush

@extends('layouts.app')
@push('scripts')
    <script type="text/javascript">
        function editUser(userId) {
            $.ajax({
                type: "GET",
                cache: false,
                url: '{{url("getUserData/")}}/' + userId,
                context: document.body,
            }).done(function (user) {
                view = '    <div class="form-group">\
                                <input type="text" id="userId" name="userId" hidden>\
                                <label for="email"><i class="icon-envelope"></i> Email</label>\
                                <input class="form-control" type="text" id="email" name="email" required>\
                            </div>\
                            <div class="form-group">\
                                <label for="email"><i class="icon-info"></i> Роль</label>\
                                <select class="form-control" name="role_id" id="role_id" >\
                                    @foreach($role as $rol)\
                                        <option value="{{$rol->id}}">{{$rol->name}}</option>\
                                    @endforeach\
                                </select>\
                            </div>\
                            <div class="form-group">\
                                <label for="password"><i class="icon-lock"></i> Пароль <small>(Пароль должен содержать от 8 до 30 знаков)</small></label>\
                                <input class="form-control" type="password" id="password" name="password">\
                            </div>\
                            <div class="form-group">\
                                <label for="password"><i class="icon-clock"></i> Дата регистрации</label>\
                                <input class="form-control" type="text" id="dataReg" name="dataReg" disabled readonly>\
                            </div>';
                $('#view').html(view);
                $('#userId').val(user.data[0].id);
                $('#email').val(user.data[0].email);
                $('#role_id').val(user.data[0].role);
                $('#dataReg').val(user.data[0].created_at);
            });
        }
    </script>
@endpush
@section('content')
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-7 align-self-center">
                <h3 class="page-title text-truncate text-dark font-weight-medium mb-1 "><i class="fa fa-users"></i> Пользователи приложения</h3>
                <div class="d-flex align-items-center">
                    Просмотр и редактирование
                </div>
            </div>
            <div class="col-5 align-self-center">
                <div class="customize-input float-right">
                   ...
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="card">
            <div class="card-body p-0">
                <div class="table-responsive">
                    <table id="dataTable" class="table table-borderless" style="width: 100%">
                        <thead>
                        <tr>
                            <th style="vertical-align: middle; text-align: center">Email<br><input type="text" class="form-control text-center form-control-sm"></th>
                            <th style="vertical-align: middle; text-align: center">Роль</th>
                            <th style="vertical-align: middle; text-align: center">Дата регистрации</th>
                            <th style="vertical-align: middle; text-align: center">Действия</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="editUser" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="border-radius: 10px">
                <div class="modal-body">
                    <h3 class="card-title" align="center">
                        Редактирование данных пользователя
                    </h3><br>
                    <form class="pl-3 pr-3" action="{{url('user/profil/editsave')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div id="view"></div>
                        <div class=" justify-content-between">
                            <button type="button" class="btn btn-outline-secondary btn-rounded float-left" data-dismiss="modal"><i class="fa fa-times-circle"></i> Отменить</button>
                            <button class="btn btn-success btn-rounded float-right active" type="submit"><i class="fa fa-save"></i> Изменить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @if(Session::get('editSuccess'))
        <input type="hidden" id="editSuccess" name="editSuccess">
    @endif
@endsection
@push('scripts')
    <script>
        if ($('#editSuccess').length > 0) {
            Swal.fire({
                type: 'success',
                title: 'Данные пользователя успешно отредактированы!',
                showConfirmButton: false,
                timer: 2000
            })
        }
        $('#dataTable').DataTable({
            "ordering": false,
            pageLength : 50,
            lengthMenu: [[50, 100, 500, -1], [50, 100, 500, 'Все']],
            "language": {
                "url": "{{ url('js/russian.json') }}"
            },
            ajax: {
                url: '{{ url('users/profilListAjax') }}',
                dataSrc: 'data'
            },
            columns: [
                {data: 'email'},
                {data: 'role', className: "select-filter"},
                {data: 'dateReg'},
                {data: 'btn'},
            ],
            "dom": "<'row'<'col-sm-5 pl-3 pt-2 text-muted'i><'col-sm-7 pr-3 pt-3'p>>"
                + "<'row'<'col-sm-12'tr>>"
                + "<'row'<'col-sm-12 text-muted pb-2 text-sm pl-3 pt-3'l>>",
            "scrollX": true,
            "createdRow": function (row) {
                $('td', row).css('vertical-align', 'middle');
                $('td:last-child', row).css('text-align', 'center');
                $('td:first-child', row).css('text-align', 'center');
                $('td:first-child+td', row).css('text-align', 'center');
            },
            initComplete: function () {
                this.api().columns('.select-filter').every(function () {
                    var column = this;
                    var select = $('<select  class="form-control form-control-sm"><option value=""></option></select>')
                        .appendTo($(column.header()))
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                    column.draw();
                });

                this.api().columns().every(function () {
                    var that = this;

                    $('input', this.header()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                .search(this.value)
                                .draw();
                        }
                    });
                });
            }
        });

    </script>
@endpush

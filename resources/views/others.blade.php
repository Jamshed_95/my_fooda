@extends('layouts.app')
@push('scripts')
    <script type="text/javascript">
        function editStatus(id) {
            $.ajax({
                type: "GET",
                cache: false,
                url: '{{url("get/statuses")}}/' + id,
                context: document.body,
            }).done(function (status) {
                view = '<input type="text" id="stat_id" name="stat_id" value="'+status.data[0].id+'" hidden>\
                        <div class="form-group">\
                            <label for="name"><i class="icon-envelope"></i> Название</label>\
                            <input class="form-control" type="text" id="name" name="name" required>\
                        </div>';
                if (status.data[0].catalog!='Где купить'){
                    view+='<div class="form-group mb-2">' +
                        '   <label for="icon"><i class="fas fa-images text-primary"></i> Иконка</label>\n' +
    '                          <div class="input-group">\n' +
    '                              <div class="custom-file">\n' +
    '                                  <input type="file" class="custom-file-input" name="icon" id="icon" accept=".png,.jpg,.ico" style="opacity: 1;">\n' +
    '                                  <label class="custom-file-label" for="icon"></label>\n' +
    '                              </div>\n' +
    '                          </div>\n' +
'                          </div>';
                }
                $('#view').html(view);
                $('#name').val(status.data[0].name);
                $('#icon').val(status.data[0].icon);
            });
        }

        function jmIcon(catalog=1){
            var view='';
            if (catalog == 3 || catalog == 1){
                var view = '<div class="form-group">\
                            <label for="status_icon"><i class="icon-picture"> </i> Иконка</label>\
                                 <div class="input-group">\
                            <div class="custom-file">\
                            <input type="file" class="custom-file-input" name="status_icon" id="status_icon" style="opacity: 1;" accept=".png,.jpg,.ico">\
                            <label class="custom-file-label" for="status_icon"></label>\
                            </div>\
                            </div>\
                            </div>'
            }
            $('#jmIcon').html(view);
        }
        $(document).ready(function (){
            jmIcon(1);
        })
    </script>
@endpush
@section('content')
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-7 align-self-center">
                <h3 class="page-title text-truncate text-dark font-weight-medium mb-1"><i class="fa fa-edit "></i> Разное</h3>
                <div class="d-flex align-items-center">
                    Добавить и редактировать
                </div>
            </div>
            <div class="col-5 align-self-center">
                <div class="customize-input float-right">
                        <button type="button" class="btn btn-outline-primary btn-rounded" data-toggle="modal" data-target="#addCategory"><i class="fas fa-plus-circle"> Добавить</i></button>
                    <div id="addCategory" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <h3 class="card-title">
                                        <i class="icon-star text-danger"> Добавить</i>
                                    </h3><br>
                                    <form class="pl-3 pr-3" action="{{url('add/new/statuses')}}" method="post" enctype="multipart/form-data">
                                        <input hidden name="type" value="addNew">
                                        @csrf
                                        <div class="form-group mb-2">
                                            <label for="category"><i class="fas fa-tasks text-primary"></i> Тип</label>
                                            <select class="form-control" name="catalog_id" id="catalog_id" onchange="jmIcon(this.value)" required>
                                                @foreach($catalogs as $catalog)
                                                    <option value="{{$catalog->id}}">{{$catalog->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nameCategory"><i class="icon-tag"> </i> Название</label>
                                            <input class="form-control" type="text" id="statusName" value="" name="statusName" required>
                                        </div>
                                        <div id="jmIcon"></div>
                                        <div class="form-group text-center">
                                            <button class="btn btn-outline-success btn-rounded mt-3" type="submit"><i class="fa fa-save"> </i> Сохранить</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <h4 class="card-title mt-2">Данные</h4>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="dataTable" class="table table-borderless" style="width: 100%">
                        <thead>
                        <tr>
                            <th style="vertical-align: middle; text-align: center">Название<br><input type="text" class="form-control text-center form-control-sm"></th>
                            <th style="vertical-align: middle; text-align: center">Тип<br></th>
                            <th style="vertical-align: middle; text-align: center">Иконка<br></th>
                            <th style="vertical-align: middle; text-align: center">Действие</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="edit_Status" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="border-radius: 10px">
                <div class="modal-body">
                    <h3 class="card-title" align="center">
                        <div id="test"></div>
                    </h3><br>
                    <form class="pl-3 pr-3" action="{{url('edit/statuses')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div id="view"></div>
                        <div class=" justify-content-between">
                            <button type="button" class="btn btn-outline-secondary btn-rounded float-left" data-dismiss="modal"><i class="fa fa-times-circle"></i> Отменить</button>
                            <button class="btn btn-success btn-rounded float-right active" type="submit"><i class="fa fa-save"></i> Изменить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @if(Session::get('deletedSuccess'))
        <input type="hidden" id="deletedSuccess">
    @endif
@endsection
@push('scripts')
    <script>
        if ($('#deletedSuccess').length > 0) {
            Swal.fire({
                type: 'error',
                title: 'Элемент успешно удален!',
                showConfirmButton: false,
                timer: 2000
            })
        }
        $('#dataTable').DataTable({
            "ordering": false,
            pageLength : 50,
            lengthMenu: [[5, 10, 50,100, -1], [5, 10, 50, 100, 'Все']],
            "language": {
                "url": "{{ url('js/russian.json') }}"
            },
            ajax: {
                url: '{{ url('get/others') }}',
                dataSrc: 'data'
            },
            columns: [
                {data: 'name'},
                {data: 'catalog', className: "select-filter"},
                {data: 'icon'},
                {data: 'btn'},
            ],
            "dom": "<'row'<'col-sm-5 pl-3 pt-2 text-muted'i><'col-sm-7 pr-3 pt-3'p>>"
                + "<'row'<'col-sm-12'tr>>"
                + "<'row'<'col-sm-12 text-muted pb-2 text-sm pl-3 pt-3'l>>",
            "scrollX": true,
            "createdRow": function (row) {
                $('td', row).css('vertical-align', 'middle');
                $('td:first-child', row).css('text-align', 'center');
                $('td:first-child+td', row).css('text-align', 'center');
                $('td:first-child+td+td', row).css('text-align', 'center');
                $('td:last-child', row).css('text-align', 'center');
            },
            initComplete: function () {
                this.api().columns('.select-filter').every(function () {
                    var column = this;
                    var select = $('<select  class="form-control form-control-sm"><option value=""></option></select>')
                        .appendTo($(column.header()))
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                    column.draw();
                });

                this.api().columns().every(function () {
                    var that = this;

                    $('input', this.header()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                .search(this.value)
                                .draw();
                        }
                    });
                });
            }
        });
    </script>
@endpush

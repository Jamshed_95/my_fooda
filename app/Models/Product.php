<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table='products';

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
    public function gdeKupit()
    {
        return $this->hasMany(Gdekupit::class, 'product_id', 'id');
    }

    public function kachestvo(){
        return $this->hasMany(Kachestva::class,'product_id','id');
    }

    public function vivod()
    {
        return $this->belongsTo(Status::class, 'vivod_id', 'id');
    }

    public function addUser()
    {
        return $this->belongsTo(User::class, 'add_user_id', 'id');
    }

    public function stattus()
    {
        return $this->belongsTo(Status::class, 'status_id', 'id');
    }

}

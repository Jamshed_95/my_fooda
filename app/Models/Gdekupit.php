<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gdekupit extends Model
{
    use HasFactory;
    protected $table = 'gdekupit';

    public function product()
    {
        return $this->belongsTo(Product::class, 'id', 'product_id');
    }
    public function status()
    {
        return $this->belongsTo(Status::class, 'gdekupit_id', 'id');
    }
}

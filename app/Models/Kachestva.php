<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kachestva extends Model
{
    use HasFactory;

    protected $table='iconkachestva';

    public function product()
    {
        return $this->belongsTo(Product::class, 'id', 'product_id');
    }
    public function status()
    {
        return $this->belongsTo(Status::class, 'statusIkKach_id', 'id');
    }
}

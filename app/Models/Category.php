<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $table = 'category';

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id'); // I believe you can use also hasOne().
    }

    public function children()
    {
        return $this->belongsTo(Category::class, 'id', 'parent_id');
    }

    public static function tree()
    {

        return static::where('parent_id', '=', 0)->get(); // or based on you question 0?

    }

    public function products()
    {
        return $this->hasManyThrough(Product::class, Category::class, 'parent_id', 'category_id', 'id');
    }

    private static function has_children($rows, $id)
    {
        foreach ($rows as $row) {
            if ($row['parent_id'] == $id)
                return true;
        }
        return false;
    }

    private static function has_product($id)
    {
        $rows = Product::where('category_id', $id)->get();
        foreach ($rows as $row) {
            if ($row['category_id'] == $id) {
                return true;
            }
        }
        return false;
    }

    private static function build_product($category_id)
    {
        $rows = Product::whereRaw('category_id='.$category_id.' AND status_id<>16')->orderBy('nameProduct')->get();
        $result = '<ul aria-expanded="false"  class="collapse  first-level base-level-line" style="margin-left: 24px; padding: 0px">';
        foreach ($rows as $row) {
            $dot='';
            if (strlen($row['nameProduct'])>40){
                $dot='...';
            }
            $result .= '<li class="sidebar-item">
                                    <a class="btn btn-sm btn-link btn-block p-0 text-left text-white" style="font-size: 10px; line-height: 27px;" href="#editForm"
                                    role="button" data-toggle="modal" title="'.$row['nameProduct'].'" onclick="editProduct('.$row->id.')" aria-expanded="false">
                                        <span class="hide-menu">' . mb_substr($row['nameProduct'],0,40, "utf-8") .$dot. '</span>
                                    </a>';
            $result .= "</li>";
        }
        $result .= "</ul>";

        return $result;
    }

    public static function build_menu($rows, $parent = 0)
    {
        $result = '<ul aria-expanded="false"  class="collapse first-level base-level-line " style="margin-left: 24px;">';
        foreach ($rows as $row) {
            if ($row['parent_id'] == $parent) {
                $haschild = self::has_children($rows, $row['id']) ? 'has-arrow' : '';
//                $href =  self::has_children($rows, $row['id']) ? url('/category/'.$row['id']) : '';
                $result .= '<li class="sidebar-item">
                                    <a class="sidebar-link  ' . $haschild . ' text-white p-0"  style="font-size: 12px; line-height: 27px;"  href="javascript:void(0)" aria-expanded="false">
                                        <i data-feather="folder" class="feather-icon"></i>
                                        <span class="hide-menu">' . $row['name'] . '</span>
                                    </a>';
                if (self::has_children($rows, $row['id'])) {
                    $result .= self::build_menu($rows, $row['id']);
                }
                $result .= "</li>";
            }
        }
        $result .= "</ul>";
        return $result;
    }

    public static function build_product_menu($rows, $parent = 0)
    {
        $result = '<ul aria-expanded="false" class="collapse first-level base-level-line " style="margin-left: 24px;">';
        foreach ($rows as $row) {
            if ($row['parent_id'] == $parent) {
                $haschild = self::has_children($rows, $row['id']) ? 'has-arrow' : (self::has_product($row['id']) ? 'has-arrow' : '');
                $result .= '<li class="sidebar-item">
                                    <a class="sidebar-link '.$haschild.' text-white p-0" href="javascript:void(0)" aria-expanded="false" style="font-size: 12px">
                                        <i data-feather="folder" class="feather-icon"></i>
                                        <span class="hide-menu">' . $row['name'] . '</span>
                                    </a>';
                if (self::has_children($rows, $row['id'])) {
                    $result .= self::build_product_menu($rows, $row['id']);
                    if (self::has_product($row['id'])) {
                        $result .= self::build_product($row['id']);
                    }
                }
                if (self::has_product($row['id'])) {
                    $result .= self::build_product($row['id']);
                }
                $result .= "</li>";
            }
        }
        $result .= "</ul>";
        return $result;
    }

    static function getDepth($parent_id) {
        $category= Category::find($parent_id);
        if ($category) {
            if ($category->parent_id == 0) {
                return $category->name;
            } else {
                return self::getDepth($category->parent_id);
            }
        }
    }

    public static function hasParent($cat_id){
        $row = Category::find($cat_id->id);
        if ($row->parent_id!=0){
            return Category::find($row->parent_id);
        }
        return false;
    }

    public static function getParent($id){
       $cat = Category::where('id',$id)->first();
        if ($cat->parent){
         $tree='';
         while($cat->parent){
             if ($tree==''){
                 $tree = $cat->parent->name;
             }else{
                   $tree=$cat->parent->name.'->'.$tree ;
             }
             $cat = $cat->parent;
         }
         return $tree;
     }
       return  '';
    }

}



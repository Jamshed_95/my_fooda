<?php

namespace App\Providers;
use App\Models\Category;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.app', function($view)
        {

            $allCategories=  Category::build_menu(Category::all()) ;
            $allCategories_product=  Category::build_product_menu(Category::all()) ;

            $view->with('categories', $allCategories);
            $view->with('product', $allCategories_product);

        });
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class PassportAuthController extends Controller
{
    public function register(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:8',
        ]);
        $check = User::where('email',$request->email)->first();
        if ($check){
            return response()->json(['error' => 'this email exist','status'=>401], 401);
        }
        $user = User::create([
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'verified' => 1,
            'role_id'  => 3
        ]);
        $token = $user->createToken('jm')->accessToken;
        return response()->json(['token' => $token,'status'=>200], 200);
    }

    public function login(Request $request)
    {
        $data = [
            'email' => $request->email,
            'password' => $request->password,
            'verified' => 1
        ];

        if (auth()->attempt($data)) {
            $token = auth()->user()->createToken('jm')->accessToken;
            return response()->json(['token' => $token,'role_id'=>auth()->user()->role_id ,'status'=>200], 200);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function logout(Request $request){
        $accessToken = Auth::user()->token();
        $token= $request->user()->tokens->find($accessToken);
        $token->revoke();
        $response=array();
        $response['status']=200;
        $response['msg']="Successfully logout";
        return response()->json($response)->header('Content-Type', 'application/json');
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class UserController extends Controller
{

    public function changePassword(Request $request){
        $this->validate($request, [
            'oldPassword' => 'required|min:8',
            'newPassword' => 'required|min:8|confirmed',
        ]);
        //newPassword_confirmation
        $user=auth()->user();
        if (Hash::check($request->oldPassword,$user->password)){
            $user->password=Hash::make($request->newPassword);
            $user->save();
            return response()->json([
                'success' => true,
                'data' => 'success'
            ]);
        }else{
            return response()->json([
                'success' => false,
                'data' => 'Old password is not match'
            ]);
        }
    }

    public function profile(){
        $user = User::select('email')->find(\auth()->user()->id);
        return response()->json([
            'success' => true,
            'data' => $user
        ]);

    }

    public function forgot() {
        $credentials = request()->validate(['email' => 'required|email']);

        Password::sendResetLink($credentials);

        return response()->json(
            [
                'success' => true,
                "msg" => 'Reset password link sent on your email id.'
            ]);
    }

    public function reset() {
        $credentials = request()->validate([
            'email' => 'required|email',
            'token' => 'required|string',
            'password' => 'required|string|confirmed'
        ]);

        $reset_password_status = Password::reset($credentials, function ($user, $password) {
            $user->password =  Hash::make($password);
            $user->save();
        });

        if ($reset_password_status == Password::INVALID_TOKEN) {
            return response()->json(["msg" => "Invalid token provided"], 400);
        }

        return redirect('/');
    }
}

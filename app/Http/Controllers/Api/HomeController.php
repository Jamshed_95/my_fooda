<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\ChosenProduct;
use App\Models\Kachestva;
use App\Models\Product;
use App\Models\User;
use App\Models\ZayavkiNaRazbor;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class HomeController extends Controller
{
    public function newInParsing(){
        $products = DB::table('products')->join('statuses','statuses.id','=','products.status_id')
                          ->select(
                              'products.nameProduct',
                              'products.imgProduct',
                              'statuses.name'
                          )
                          ->where('products.status_id','=',13)
                          ->orderby('products.created_at','desc')
                          ->get();

        $products = json_decode(json_encode($products),true);
        foreach ($products as &$product){
            $product['imgProduct']= url('/imgProduct/'.$product['imgProduct']);
        }
        unset($product);
        return response()->json([
            'success' => true,
            'data' => $products
        ]);
    }

    public function sendToParsing(Request  $request){
        $validator = Validator::make($request->all(),[
            'file' => 'required|mimes:jpg,png|max:10096',
            'file2' => 'required|mimes:jpg,png|max:10096',
            'file3' => 'required|mimes:jpg,png|max:10096',
        ]);

        if($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
            $file= $request->file('file');
            $file2= $request->file('file2');
            $file3= $request->file('file3');
            $name = 'product_'.date('dmYs').'.'.$file->guessClientExtension();
            $name2 = 'product_'.date('dmsY').'.'.$file2->guessClientExtension();
            $name3 = 'product_'.date('sdmY').'.'.$file3->guessClientExtension();

            //store your file into directory and db
            $save = new ZayavkiNaRazbor();
            $save->koment = $request->comment;
            $save->img1= $name;
            $save->img2= $name2;
            $save->img3= $name3;
            $save->add_user_id= auth()->user()->id;
            $save->save();
            $file->storeAs('order/'.$save->id, $name);
            $file2->storeAs('order/'.$save->id, $name2);
            $file3->storeAs('order/'.$save->id, $name3);
            return response()->json([
                "success" => true,
                "message" => "File successfully uploaded",
            ]);
    }

    public function saveChosen(Request $request){
            $this->validate($request,[
                'product_id'=>'required'
            ]);
            $check = ChosenProduct::where('product_id',$request->product_id)
                                  ->where('user_id',auth()->user()->id)->first();
            if ($check) {
                if ($check->status == 1) {
                    return response()->json([
                        "success" => false,
                        "message" => "Product already in chosen",
                    ]);
                } else {
                    $check->status = 1;
                    $check->save();
                    return response()->json([
                        "success" => true,
                        "message" => "Product successfully in chosen",
                    ]);
                }
            } else {
                $chose = new ChosenProduct();
                $chose->product_id = $request->product_id;
                $chose->user_id = auth()->user()->id;
                $chose->save();

                return response()->json([
                    "success" => true,
                    "message" => "Product successfully in chosen",
                ]);
            }
    }

    public function getChosen(){
            $chosens = DB::table('products')->join('chosen_products','chosen_products.product_id','=','products.id')
                ->select(
                    'products.*'
                )
                ->where('chosen_products.status',1)->where('chosen_products.user_id',auth()->user()->id)->get();
        $chosens = json_decode(json_encode($chosens),true);
        foreach ($chosens as &$chosen){
            $chosen['imgProduct']= url('/imgProduct/'.$chosen['imgProduct']);
        }
        unset($chosen);
        return response()->json([
            "success" => true,
            "data" => $chosens,
        ]);
    }

    public function delChosen(Request $request){
        $this->validate($request,[
            'product_id'=>'required'
        ]);
        $chose = ChosenProduct::where('product_id',$request->product_id)->where('user_id',auth()->user()->id)->first();
        if ($chose){
            if ($chose->status==1){
                $chose->status=0;
                $chose->save();
                return response()->json([
                    "success" => true,
                    "message" => "Product successfully deleted from chosen",
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Product deleted yet from chosen!",
                ]);
            }

        }else{
            return response()->json([
                "success" => false,
                "message" => "Product not found in chosen!",
            ]);
        }

    }

    public function getProduct(){
        $products = DB::table('products')
            ->join('category','category.id','products.category_id')
            ->leftJoin('statuses','statuses.id','products.vivod_id')
            ->select(
                'category.id as category_id',
                'category.name as category_name',
                'products.*',
                'products.vivod_id',
                'statuses.name as vivod',
                'statuses.icon as vivod_icon'
            )
            ->where('products.status_id','=',13)->get();
        $products = json_decode(json_encode($products),true);
            foreach ($products as &$product){
                $icona = DB::table('iconkachestva')
                    ->join('statuses','statuses.id','iconkachestva.statusIkKach_id')
                    ->select(
                        'statuses.name as icon_name',
                        'statuses.icon as icon'
                    )
                    ->where('product_id',$product['id'])->get();

                $gdekupit = DB::table('gdekupit')
                    ->join('statuses','statuses.id','gdekupit.gdekupit_id')
                    ->select(
                        'statuses.name as gde_kupit',
                        'statuses.icon as gde_kupit_icon'
                    )
                    ->where('product_id',$product['id'])->get();

                $icona = json_decode(json_encode($icona),true);
                $product['imgProduct']=url('/imgProduct/'.$product['imgProduct']);
                if($product['vivod_id']){
                    $product['vivod_icon']=$product['vivod_icon']?url('/imgStatus/'.$product['vivod_icon']):url('/imgStatus/no-image.png');

                        if($product['vivod']=='Хороший состав' || $product['vivod']=='Хороший состав, но с сахаром'){
                            $product['vivod_color'] = 'green';
                        }elseif ($product['vivod']=='Состав с замечаниями' || $product['vivod']=='Есть замечания'){
                            $product['vivod_color'] = 'yellow';
                        }elseif ($product['vivod']=='Не рекомендую' || 'Не рекомендовано'){
                            $product['vivod_color'] = 'red';
                        }else{
                            $product['vivod_color'] = null;
                        }
                }
                foreach($icona as &$icon){
                    $icon['icon']= $icon['icon']?url('/imgStatus/'.$icon['icon']):url('/imgStatus/no-image.png');
                }
                unset($icon);
               $product['icon']=$icona;

                $gdekupit = json_decode(json_encode($gdekupit),true);
                foreach($gdekupit as &$gde){
                    $gde['gde_kupit_icon']= $gde['gde_kupit_icon']?url('/imgStatus/'.$gde['gde_kupit_icon']):url('/imgStatus/no-image.png');
                }
                unset($gde);
                $product['gdekupit']=$gdekupit;
            }
            unset($product);
        return response()->json([
            "success" => true,
            "data" => $products,
        ]);

    }

    public function getCategory(){
        $category = Category::all();
        foreach($category as $item){
            $item['img_name'] = $item['img_name']!=null?url('imgCategory/'.$item['img_name']):url('imgCategory/no-image.png');
            $childs[$item->parent_id][] = $item;
        }
        foreach($category as &$item) {
            if (isset($childs[$item->id])){
                $item->childs = $childs[$item->id];
            }
        }
        $tree = $childs[0];
        $category = json_decode(json_encode($tree),true);
        return response()->json([
            "success" => true,
            "data" => $category,
        ]);


    }

    public function getDesignation(){
        $designations = DB::select('SELECT
                                              s.id,
                                              s.name,
                                              s.icon,
                                              sc.name catalog
                                            FROM statuses s
                                              INNER JOIN status_catalog sc ON sc.id=s.catalog_id
                                            WHERE sc.id IN (1,3)');
        $designations = json_decode(json_encode($designations),true);
        foreach ($designations as &$designation){
            $designation['icon'] = $designation['icon']?url('/imgStatus/'.$designation['icon']):url('/imgStatus/no-image.png');
        }
        unset($designation);
        return response()->json([
            "success" => true,
            "data" => $designations,
        ]);
    }

    public function getOneProduct(Request $request){
        $this->validate($request,[
            'product_id'=>'required'
        ]);

        $product = DB::table('products')
            ->join('category','category.id','products.category_id')
            ->leftJoin('statuses','statuses.id','products.vivod_id')
            ->select(
                'category.id as category_id',
                'category.name as category_name',
                'products.*',
                'products.vivod_id',
                'statuses.name as vivod',
                'statuses.icon as vivod_icon'
            )
            ->where('products.status_id','=',13)
            ->where('products.id','=',$request->product_id)
            ->first();
        $product = json_decode(json_encode($product),true);
        $icona = DB::table('iconkachestva')
            ->join('statuses','statuses.id','iconkachestva.statusIkKach_id')
            ->select(
                'statuses.name as icon_name',
                'statuses.icon as icon'
            )
            ->where('product_id',$product['id'])->get();

        $gdekupit = DB::table('gdekupit')
            ->join('statuses','statuses.id','gdekupit.gdekupit_id')
            ->select(
                'statuses.name as gde_kupit',
                'statuses.icon as gde_kupit_icon'
            )
            ->where('product_id',$product['id'])->get();

        $icona = json_decode(json_encode($icona),true);
        $product['imgProduct']=url('/imgProduct/'.$product['imgProduct']);
        if($product['vivod_icon']){
            $product['vivod_icon']=$product['vivod_icon']?url('/imgStatus/'.$product['vivod_icon']):url('/imgStatus/no-image.png');

            if($product['vivod']=='Хороший состав' || $product['vivod']=='Хороший состав, но с сахаром'){
                $product['vivod_color'] = 'green';
            }elseif ($product['vivod']=='Состав с замечаниями' || $product['vivod']=='Есть замечания'){
                $product['vivod_color'] = 'yellow';
            }elseif ($product['vivod']=='Не рекомендую' || 'Не рекомендовано'){
                $product['vivod_color'] = 'red';
            }else{
                $product['vivod_color'] = null;
            }
        }
        foreach($icona as &$icon){
            $icon['icon']= $icon['icon']?url('/imgStatus/'.$icon['icon']):url('/imgStatus/no-image.png');
        }
        unset($icon);
        $product['icon']=$icona;

        $gdekupit = json_decode(json_encode($gdekupit),true);
        foreach($gdekupit as &$gde){
            $gde['gde_kupit_icon']= $gde['gde_kupit_icon']?url('/imgStatus/'.$gde['gde_kupit_icon']):url('/imgStatus/no-image.png');
        }
        unset($gde);
        $product['gdekupit']=$gdekupit;
        return response()->json([
            "success" => true,
            "data" => $product,
        ]);

    }

    public function getProductByCategory(Request $request){
        $this->validate($request,[
            'category_id'=>'required'
        ]);

        $category = DB::table('category')
            ->join('products','category.id','products.category_id')
            ->leftJoin('statuses','statuses.id','products.vivod_id')
            ->select(
                'category.id as category_id',
                'category.name as category_name',
                'products.*',
                'products.vivod_id',
                'statuses.name as vivod',
                'statuses.icon as vivod_icon'
            )
            ->where('products.status_id','=',13)
            ->where('category.id','=',$request->category_id)
            ->get();
        $category = json_decode(json_encode($category),true);
        foreach ($category as &$product){
            $icona = DB::table('iconkachestva')
                ->join('statuses','statuses.id','iconkachestva.statusIkKach_id')
                ->select(
                    'statuses.name as icon_name',
                    'statuses.icon as icon'
                )
                ->where('product_id',$product['id'])->get();

            $gdekupit = DB::table('gdekupit')
                ->join('statuses','statuses.id','gdekupit.gdekupit_id')
                ->select(
                    'statuses.name as gde_kupit',
                    'statuses.icon as gde_kupit_icon'
                )
                ->where('product_id',$product['id'])->get();

            $icona = json_decode(json_encode($icona),true);
            $product['imgProduct']=url('/imgProduct/'.$product['imgProduct']);
            if($product['vivod_id']){
                $product['vivod_icon']=$product['vivod_icon']?url('/imgStatus/'.$product['vivod_icon']):url('/imgStatus/no-image.png');

                if($product['vivod']=='Хороший состав' || $product['vivod']=='Хороший состав, но с сахаром'){
                    $product['vivod_color'] = 'green';
                }elseif ($product['vivod']=='Состав с замечаниями' || $product['vivod']=='Есть замечания'){
                    $product['vivod_color'] = 'yellow';
                }elseif ($product['vivod']=='Не рекомендую' || 'Не рекомендовано'){
                    $product['vivod_color'] = 'red';
                }else{
                    $product['vivod_color'] = null;
                }
            }
            foreach($icona as &$icon){
                $icon['icon']= $icon['icon']?url('/imgStatus/'.$icon['icon']):url('/imgStatus/no-image.png');
            }
            unset($icon);
            $product['icon']=$icona;

            $gdekupit = json_decode(json_encode($gdekupit),true);
            foreach($gdekupit as &$gde){
                $gde['gde_kupit_icon']= $gde['gde_kupit_icon']?url('/imgStatus/'.$gde['gde_kupit_icon']):url('/imgStatus/no-image.png');
            }
            unset($gde);
            $product['gdekupit']=$gdekupit;
        }
        unset($product);

        return response()->json([
            "success" => true,
            "data" => $category,
        ]);

    }

    public function getCreateSubscriber(Request $request){
        $client = new Client();
        $apiKey = 'rgUZabaKwMXrvPGqSCKynmNyNwNzBQIF';
        $response = $client->request('GET', 'https://api.revenuecat.com/v1/subscribers/'.$request->user_id, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer '.$apiKey,
                'Content-Type' => 'application/json',
                'X-Platform'=>'android'
            ],
        ]);
        $result =  $response->getBody();
        $result= json_decode($result);
        $t = 'Access to the application';
        if (isset($result->subscriber->entitlements->$t)){
             $resultDate = Carbon::parse($result->subscriber->entitlements->$t->expires_date);
             if($resultDate < Carbon::now()) {
                 return response()->json([
                     "error" => true,
                     "data" => 'Subscription is no longer valid',
                 ]);
             }
         }
        return  $result;
    }
}

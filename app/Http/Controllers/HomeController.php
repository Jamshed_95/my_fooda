<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Gdekupit;
use App\Models\Kachestva;
use App\Models\Product;
use App\Models\Role;
use App\Models\Status;
use App\Models\StatusCatalog;
use App\Models\User;
use App\Models\ZayavkiNaRazbor;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use function Symfony\Component\String\b;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function category($id = 0)
    {
        $category = Category::all();
        $allCategories = Category::build_menu(Category::orderBy('name')->get());
        $superAdmin=false;
        if (Auth::user()->role->name=='Супер Админ'){
            $superAdmin=true;
        }
        return view('category', ['cat_id' => $id, 'category' => $category, 'allCategories' => $allCategories, 'superAdmin' => $superAdmin]);
    }

    public function profil()
    {
        $user = User::find(Auth::user()->id);
        return view('user.profil', compact('user'));
    }

    public function getUser($userId)
    {
        $user = User::find($userId);
        return $user;
    }

    public function editUserSave(Request $request)
    {
        $user = User::find($request->userId);
        if ($user) {
            $user->email = $request->emailaddress;
            if ($request->password != null) {
                $user->password = Hash::make($request->password);
            }
            $user->save();
            return back()->with('successEdit', 'qq');
        } else {
            return back()->with('notUser', 'qq');
        }
    }

    public function categoryListAjax($cat_id)
    {
        if ($cat_id == 0) {
            $lists = Category::where('status', 1)->orderBy('name')->get();
        } else {
            $lists = Category::where('status', 1)->whereRaw('id=' . $cat_id . ' or parent_id=' . $cat_id)->orderBy('name')->get();
        }
        if (count($lists) > 0) {
            foreach ($lists as $data) {
                $img1Url = $data->img_name == '' ? '/no-image.png' : $data->img_name;
                $img = '<img src="' . url('imgCategory/' . $img1Url) . '" class="" style="width: 40%"/>';
                $btnEdit = '<a title="Удалить категорию" href="' . url('deleteCategory/' . $data->id) . '" class="btn btn-danger" onclick="return confirm(\'Удалить выбранный каталог? \nЕсли Вы удалите каталог, то удаляется и продукты связанные с этим каталогом!!!\')")">
                                    <i class="icon-trash m-0"> </i>
                                </a> ';
                $btnNameCat=' <a href="#editCategory" class="btn btn-outline-primary btn-block btn-rounded" onclick="editCategory(' . $data->id . ')"
                                    role="button" data-toggle="modal" title="Редактировать">'.$data->name.'</a>';
                $potomki=$data->getParent($data->id);

                $result['data'][] = [
                    "img" => $img,
                    "nameCategory" => $btnNameCat,
                    "potomki" => $potomki,
                    "btn" => $btnEdit,
                ];
            }
        } else {
            $result['data'][] = [
                "img" => 'нет данных',
                "nameCategory" => 'нет данных',
                "btn" => 'нет данных',
            ];
        }
        return response()->json($result);
    }

    public function categorySave(Request $request)
    {
        if ($request->type == 'addNew') {
            $validated = $request->validate([
                'nameCategory' => 'required|string|max:255',
                'imgCategry' => 'file',
            ]);
        } elseif ($request->type == 'edit') {
            $validated = $request->validate([
                'nameCategory' => 'required|string|max:255',
                'imgCategry' => 'file',
            ]);
        }

        if ($validated) {
            if ($request->type == 'addNew') {
                $cat = new Category();
                $msg = 'successSaveCategory';
            } elseif ($request->type == 'edit') {
                $cat = Category::find($request->categoryId);
                $msg = 'successEditCategory';
            }
            $cat->name = $request->nameCategory;
            if ($request->imgCategry) {
                $fileName = Carbon::now()->format('Ymd') . '_' . $request->imgCategry->getClientOriginalName();
                $request->imgCategry->move(public_path('imgCategory'), $fileName);
                $cat->img_name = $fileName;
            }
            $cat->add_user_id = Auth::user()->id;
            $parent_id= $request->category==null?0:$request->category;
            $cat->parent_id =$parent_id;
            $cat->save();
            return back()->with($msg, 'ok');
        } else {
            return back()->with('errorSaveCategory', 'ok');
        }
    }

    public function deleteCategory($category_id)
    {
        $categ = Category::where('id', $category_id)->first();
        $categ->delete();
        $pr=Product::where('category_id',$category_id)->first();
        if ($pr){
            $pr->delete();
        }
        return back()->with('deletedSuccess', 'Успешно!');
    }

    public function getCategory($category_id)
    {
        $cate = Category::find($category_id);
        return $cate;
    }

    public function product($status)
    {
        $category = Category::all();
        $gdeKupitSp = Status::where('catalog_id', 2)->get();
        $ikonkaKachestva = Status::whereRaw('catalog_id=1 ')->get();
        $vivod = Status::where('catalog_id', 3)->get();
        $statuS = Status::where('catalog_id', 4)->get();
        $allCategories_product = Category::build_product_menu(Category::orderBy('name')->get());
        $infoStatus=DB::select('SELECT
                                          s.name AS statusname,
                                          s.icon AS statusicon,
                                          COUNT(p.id) AS countt
                                        FROM statuses s
                                          LEFT JOIN products p ON p.status_id=s.id
                                        WHERE s.catalog_id=4
                                        GROUP BY
                                         s.id');
        return view('product', compact('gdeKupitSp', 'ikonkaKachestva', 'vivod', 'category', 'status', 'allCategories_product','statuS','infoStatus'));
    }

    public function addProductSave(Request $request)
    {
        if ($request->type=='newAdd') {
            $product = new Product();
        }elseif ($request->type=='edit'){
            $product = Product::find($request->idProduct);
        }
        $product->category_id = $request->categoryId;
        $product->nameProduct = $request->nameProduct;
        $product->dopOpisanie = $request->dopOpisanie;
        $product->proizvoditel = $request->proizvoditel;
        $product->sostav = $request->sostav;
        $product->nezhelDobavki = $request->nezhelDobavki;
        $product->kkal = $request->kkal;
        $product->belki = $request->belki;
        $product->zhir = $request->zhir;
        $product->uglevodi = $request->uglevodi;
        $product->sahar = $request->sahar;
        $product->kletchatka = $request->kletchatka;
        $product->shtrihkod = $request->shtrihkod;
        $product->vivod_id = $request->vivod;
        $product->status_id = $request->statuS;
        $product->add_user_id = Auth::user()->id;
        if ($request->imgProduct) {
            $fileName = Carbon::now()->format('Ymd') . '_' . $request->imgProduct->getClientOriginalName();
            $request->imgProduct->move(public_path('imgProduct'), $fileName);
            $product->imgProduct = $fileName;
        }
        $product->save();

        if ($request->ikKachestva){
            if ($request->type=='newAdd') {
                foreach ($request->ikKachestva as $icc){
                    $iconKach=new Kachestva();
                    $iconKach->product_id=$product->id;
                    $iconKach->statusIkKach_id=$icc;
                    $iconKach->save();
                }
            }elseif ($request->type=='edit'){
                $iconKach = Kachestva::where('product_id',$request->idProduct)->get();
                foreach ($iconKach as $iconKach11){
                    $iconKach11->delete();
                }
                foreach ($request->ikKachestva as $icc){
                    $iconKach=new Kachestva();
                    $iconKach->product_id=$request->idProduct;
                    $iconKach->statusIkKach_id=$icc;
                    $iconKach->save();
                }
            }
        }else{
            if ($request->type=='edit'){
                $iconKach = Kachestva::where('product_id',$request->idProduct)->get();
                foreach ($iconKach as $iconKach11){
                    $iconKach11->delete();
                }
            }
        }
        if ($request->gdeKupit){
            if ($request->type=='newAdd') {
                foreach ($request->gdeKupit as $icc){
                    $gdeKupit=new Gdekupit();
                    $gdeKupit->product_id=$product->id;
                    $gdeKupit->gdekupit_id=$icc;
                    $gdeKupit->save();
                }
            }elseif ($request->type=='edit'){
                $gdeKupit = Gdekupit::where('product_id',$request->idProduct)->get();
                foreach ($gdeKupit as $gdeKupit11){
                    $gdeKupit11->delete();
                }
                foreach ($request->gdeKupit as $icc){
                    $iconKach=new Gdekupit();
                    $iconKach->product_id=$request->idProduct;
                    $iconKach->gdekupit_id=$icc;
                    $iconKach->save();
                }
            }
        }
        return back();
    }

    public function productListAjax($status)
    {
        if($status=='all'){
            $status = '13,14,15';
        }
        $sql=' SELECT
                   p.id,
                   p.nameProduct,
                   c.name AS nameCategory,
                   p.proizvoditel,
                   p.sostav,
                   p.sahar,
                   sVivod.name AS vivod,
                   sVivod.icon AS vivodIcon,
                   p.status_id,
                   sStatus.order_item AS statusName,
                   sStatus.icon AS statusIcon
                FROM products p
                LEFT JOIN category c ON c.id=p.category_id
                LEFT JOIN statuses sVivod ON sVivod.id=p.vivod_id
                LEFT JOIN users u ON u.id=p.add_user_id
                LEFT JOIN statuses sStatus ON sStatus.id=p.status_id
                WHERE p.status_id IN ('.$status.')
                ORDER BY p.created_at DESC';
        $lists = DB::select($sql);
        if (count($lists) > 0) {
            foreach ($lists as $data) {
                if ($data->status_id==13 ||$data->status_id==14 ||$data->status_id==15){
                    $btnIcon='icon-trash';
                    $btnType='warning';
                    $btnTitle='В корзину';
                }else{
                    $btnIcon='icon-close';
                    $btnType='danger';
                    $btnTitle='Удалить';
                }
                $btn='
                 <a title="'.$btnTitle.'" href="' . url('deleteProduct/' . $data->id) . '" class="btn btn-'.$btnType.' btn-block">
                    <i class="'.$btnIcon.'"></i>
                </a>
                ';
                $btnNameProd='<a href="#editForm" onclick="editProduct(' . $data->id . ')"
                                role="button" data-toggle="modal" title="Редактировать">'.$data->nameProduct.'</a>';
                $btnNameCat='<a href="#editForm" onclick="editProduct(' . $data->id . ')"
                                role="button" data-toggle="modal" title="Редактировать">'.$data->nameCategory.'</a>';
                if ($data->vivodIcon==null){
                    $viv='';
                }else{
                    $urlVivodIcon=$data->vivodIcon==null?'no-image.png':$data->vivodIcon;
                    $viv='<img class="img-fluid" style="width:15%; height:15%" src="'.url('imgStatus/'.$urlVivodIcon).'"> '.$data->vivod;
                }
                $result['data'][] = [
                    "nameProduct" =>$btnNameProd,
                    "categoryName" => $btnNameCat,
                    "proizvoditel" => $data->proizvoditel,
                    "sostav" => $data->sostav,
                    "sahar" => $data->sahar,
                    "statusName" => '<i class="'.$data->statusIcon.'"></i> '.$data->statusName,
                    "vivod" => $viv,
                    "btn" => $btn
                ];
            }
        } else {
            $result['data'][] = [
                "nameProduct" => 'нет данных',
                "categoryName" => 'нет данных',
                "proizvoditel" => 'нет данных',
                "sostav" => 'нет данных',
                "sahar" => 'нет данных',
                "statusName" => 'нет данных',
                "vivod" => 'нет данных',
                "btn" => 'нет данных',
            ];
        }
        return response()->json($result);
    }

    public function deleteProduct($product_id){
        $product=Product::find($product_id);
        if ($product){
            if ($product->status_id==13 || $product->status_id==14 || $product->status_id==15){
                $product->status_id=16;
                $product->save();
                return back()->with('addToTrash','ok');
            } elseif ($product->status_id==16){
                $product->delete();
                return back()->with('deleted_success','ok');
            }
        }else{
            return back()->with('deleted_error','ok');
        }
    }

    public function getProduct($product_id){
        $prod = Product::find($product_id);
         if ($prod->kachestvo){
             foreach ($prod->kachestvo as $key=>$kachestvo)
             $prod['kachestvo'][$key]=$kachestvo->statusIkKach_id;
         }

        if ($prod->gdeKupit){
            foreach ($prod->gdeKupit as $key=>$gde)
                $prod['gdeKupit'][$key]=$gde->gdekupit_id;
        }
        return $prod;
    }

    public function zayavkiNaRazbor()
    {
        return view('zayavkiNaRazbor');
    }

    public function zayavkiNaRazborAjax()
    {
        $lists = ZayavkiNaRazbor::where('status', 1)->orderBy('created_at', 'DESC')->get();
        if (count($lists) > 0) {
            foreach ($lists as $data) {
                $btn = '
                            <a title="Принято" href="' . url('zayavkaNaRazborPrinyat/' . $data->id) . '" class="btn btn-success btn-block">
                                <i class="icon-check "></i>
                            </a>
                    ';
                $img1Url = $data->img1 == '' ? 'no-image.png' : $data->img1;
                $img2Url = $data->img2 == '' ? 'no-image.png' : $data->img2;
                $img3Url = $data->img3 == '' ? 'no-image.png' : $data->img3;
                $img1 = '<img src="' . url('../storage/app/order/' . $data->id . '/' . $img1Url) . '" class="" style="width: 50%"/>';
                $img2 = '<img src="' . url('../storage/app/order/' . $data->id . '/' . $img2Url) . '" class="" style="width: 50%"/>';
                $img3 = '<img src="' . url('../storage/app/order/' . $data->id . '/' . $img3Url) . '" class="" style="width: 50%"/>';
                $result['data'][] = [
                    "img1" => $img1,
                    "img2" => $img2,
                    "img3" => $img3,
                    "shtrihkod" => '-',
                    "comment" => $data->koment,
                    "btn" => $btn,
                ];
            }
        } else {
            $result['data'][] = [
                "img1" => 'нет данных',
                "img2" => 'нет данных',
                "img3" => 'нет данных',
                "shtrihkod" => 'нет данных',
                "comment" => 'нет данных',
                "btn" => 'нет данных',
            ];
        }
        return response()->json($result);
    }

    public function zayavkaNaRazborPrinyat($id)
    {
        $zayavka = ZayavkiNaRazbor::find($id);
        $zayavka->status = 0;
        $zayavka->save();
        return back()->with('prinat', 'ok');
    }

    public function usersProfil()
    {
        $role=Role::all();
        return view('usersProfil', compact('role'));
    }

    public function usersProfilListAjax()
    {
        $lists = User::orderBy('created_at', 'DESC')->get();
        if (count($lists) > 0) {
            foreach ($lists as $data) {
                $btn = '
                            <a href="#editUser" class="btn btn-primary btn-block " onclick="editUser(\'' . $data->id . '\')"
                                role="button" data-toggle="modal" title="Редактировать"> <i class="icon-pencil m-0"> </i>
                            </a>
                        ';
                $result['data'][] = [
                    "email" => $data->email,
                    "role" => $data->role->name,
                    "dateReg" => Carbon::parse($data->created_at)->format('d.m.Y h:i:s'),
                    "btn" => $btn,
                ];
            }
        } else {
            $result['data'][] = [
                "email" => 'нет данных',
                "tel" => 'нет данных',
                "dateReg" => 'нет данных',
                "btn" => 'нет данных',
            ];
        }
        return response()->json($result);
    }

    public function getUserData($user_id)
    {
        $user = User::find($user_id);
        $result['data'] = array();
        $result['data'][] = [
            "id" => $user->id,
            "email" => $user->email,
            "role" => $user->role_id,
            "created_at" => Carbon::parse($user->created_at)->format('d.m.Y в h:i:s'),
        ];
        return response()->json($result);
    }

    public function userEditSave(Request $request)
    {
        $user = User::find($request->userId);
        $user->email = $request->email;
        if ($user->role_id==1 || $user->role_id==2){
            $user->role_id = $request->role_id;
            $user->verified =1;
        }else{
            $user->role_id = $request->role_id;
        }
        if ($request->password != null) {
            $user->password = Hash::make($request->password);
        }
        $user->save();
        return back()->with('editSuccess', 'ok');
    }

    public function zaprosiNaReg()
    {
        return view('zaprosiNaReg');
    }

    public function getuserZaprosiNaReg()
    {
        $lists = User::where('verified', 0)->orderBy('created_at', 'DESC')->get();
        if (count($lists) > 0) {
            foreach ($lists as $data) {
                $btn = '
                            <a href="'.url('acceptUserForLogin/1/'.$data->id).'" class="btn btn-success" title="Принять">
                                <i class="fa fa-user-secret m-0"> Супер-админ </i>
                            </a>
                            <a href="'.url('acceptUserForLogin/2/'.$data->id).'" class="btn btn-primary" title="Принять">
                                <i class="fa fa-user-circle m-0"> Админ </i>
                            </a>
                            <a href="'.url('acceptUserForLogin/3/'.$data->id).'" class="btn btn-warning" title="Принять">
                                <i class="fa fa-user m-0"> Пользователь </i>
                            </a>
                ';
                /*$btn = '<form method="post" action="' . url('acceptUserForLogin') . '/' . $data->id . '">
                            ' . csrf_field() . '

                            <button type="submit" class="btn btn-success btn-block " title="Принять"> <i class="fa fa-check-circle m-0"> </i></button>
                        </form> ';*/
                $result['data'][] = [
                    "email" => $data->email,
                    "dataOtpr" => Carbon::parse($data->created_at)->format('d.m.Y h:i:s'),
                    "btn" => $btn,
                ];
            }
        } else {
            $result['data'][] = [
                "email" => 'нет данных',
                "dataOtpr" => 'нет данных',
                "btn" => 'нет данных',
            ];
        }
        return response()->json($result);
    }

    public function acceptUserForLogin($role_id,$user_id)
    {
        $userr = User::find($user_id);
        $userr->verified = 1;
        $userr->role_id = $role_id;
        $userr->save();
        return back();
    }

    public function addNewIconKachestva(Request $request){
        $stat=new Status();
        $stat->name=$request->name;
        if ($request->img){
            $fileName = Carbon::now()->format('Ymd') . '_' . $request->img->getClientOriginalName();
            $request->img->move(public_path('imgStatus'), $fileName);
            $stat->icon = $fileName;
        }
        $stat->catalog_id=1;
        $stat->save();
        return back()->with('added', 'ok');
    }

    public function showOthers(){
        $catalog = StatusCatalog::whereRaw('id in (1,2,3)')->get();
        return view('others',['catalogs'=>$catalog]);
    }

    public function getOthers_ajax(){
        $lists = DB::table('status_catalog')
                   ->join('statuses','statuses.catalog_id','=','status_catalog.id')
                   ->select(
                        'status_catalog.name as catalog',
                                'statuses.*'
                            )
                   ->whereRaw('status_catalog.id in (1,2,3)')
                   ->orderBy('status_catalog.name')
                   ->get();
        if (count($lists) > 0) {
            foreach ($lists as $data) {
                $img1Url = $data->icon == '' ? '/no-image.png' :$data->icon;
                $icon =  '<img style="width:8%;" src="'.url('imgStatus/'.$img1Url).'"><br> ';
                $btn = '
                            <a href="#edit_Status" class="btn btn-primary" onclick="editStatus(' . $data->id . ')"
                                role="button" data-toggle="modal" title="Редактировать"> <i class="icon-pencil m-0"> </i>
                            </a>
                            <a href="'.url('otherDelete/'.$data->id).'" class="btn btn-danger" title="Удалить"
                                    onclick="return confirm(\'Удалить выбранный статус? \nЕсли Вы удалите, то удаляются все данные связанные с этим статусом!!!\')">
                                <i class="icon-trash m-0"> </i>
                            </a>
                        ';
                $result['data'][] = [
                    "catalog" => $data->catalog,
                    "name" => $data->name,
                    "icon" => $icon,
                    "btn" => $btn,
                ];
            }
        } else {
            $result['data'][] = [
                "catalog" => 'нет данных',
                "name" => 'нет данных',
                "icon" => 'нет данных',
                "btn" => 'нет данных',
            ];
        }
        return response()->json($result);
    }

    public function getStatuses($id){
        $lists = DB::table('status_catalog')
            ->join('statuses','statuses.catalog_id','=','status_catalog.id')
            ->select(
                'status_catalog.name as catalog',
                'statuses.*'
            )
            ->where('statuses.id',$id)
            ->get();

        $result['data'] = array();
        foreach ($lists as $status){
            $result['data'][] = [
                "id" => $status->id,
                "name" => $status->name,
                "catalog" => $status->catalog,
                "icon" => $status->icon,
            ];
        }

        return response()->json($result);
    }

    public function editStatues(Request $request){
       $status = Status::find($request->stat_id);
       $status->name = $request->name;
        if ($request->icon) {
            $fileName = Carbon::now()->format('Ymd') . '_' . $request->icon->getClientOriginalName();
            $request->icon->move(public_path('imgStatus'), $fileName);
            $status->icon = $fileName;
        }
       $status->save();
      return back();
    }

    public function addNewStatus(Request $request){
       $status = new  Status();
       $status->name =$request->statusName;
       $status->catalog_id =$request->catalog_id;
        if ($request->hasFile('status_icon')) {
            $fileName = Carbon::now()->format('Ymd') . '_' . $request->status_icon->getClientOriginalName();
            $request->status_icon->move(public_path('imgStatus'), $fileName);
            $status->icon = $fileName;
        }
        $status->save();
        return back();
    }

    public function otherDelete($data_id){
        $data=Status::find($data_id);
        if ($data->catalog_id==1){
            $kachestva=Kachestva::where('statusIkKach_id',$data->id)->get();
            foreach ($kachestva as $kachest){
                $kachest->delete();
            }
            $data->delete();
        }else{
            $data->delete();
        }
        return back()->with('deletedSuccess', 'Успешно!');
    }

}
